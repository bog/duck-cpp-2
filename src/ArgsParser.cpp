#include <ArgsParser.hpp>

ArgsParser::ArgsParser(std::vector<std::string> args)
  : m_args { args }
{
  
}

std::string ArgsParser::getAction() const
{
  if ( m_args.empty() )
    {
      return "";
    }
  
  return m_args.front();
}

std::vector<std::string> ArgsParser::getParams() const
{
  if (!m_args.empty())
    {
      std::vector<std::string> res(m_args.size() - 1, "");
      std::copy(m_args.begin() + 1, m_args.end(), res.begin());
      return res;
    }
  
  return std::vector<std::string>();
}


size_t ArgsParser::size() const
{
  return m_args.size();
}

ArgsParser::~ArgsParser()
{
  
}
