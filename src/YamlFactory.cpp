#include <sstream>
#include <YamlFactory.hpp>

YamlFactory::YamlFactory()
{
  
}

std::string YamlFactory::simple(std::string const& project_name)
{
  std::stringstream output;
  output << "project:" << std::endl;
  output << "    " << "name: " << project_name << std::endl;
  output << "    " << "authors:" << std::endl;
  output << "    " << "license:" << std::endl;
  output << "    " << "version:" << std::endl;

  output << "install:" << std::endl;
  output << "    " << "bin-dir: /usr/local/bin" << std::endl;
  output << "    " << "asset-dir: /usr/local/share/" << project_name << std::endl;

  output << "targets:" << std::endl;
  output << "    " << "- target:" << std::endl;
  output << "        " << "name: gnu64" << std::endl;
  output << "        " << "exec-path: build/bin/"<< project_name << std::endl;
  output << "        " << "obj-dir: build/obj" << std::endl;
  output << "        " << "sources:" << std::endl;
  output << "            " << "- src" << std::endl;
  output << "        " << "includes:" << std::endl;
  output << "            " << "- src" << std::endl;
  
  return output.str();
}

YamlFactory::~YamlFactory()
{
  
}
