#include <YamlReader.hpp>

YamlReader::YamlReader(std::string const& sources)
{
  m_node = YAML::Load(sources);
}


Project YamlReader::readProject() const
{  
  Project project;

  if (!m_node["project"])
    {
      return project;
    }

  if (m_node["project"]["name"] &&
      m_node["project"]["name"].Type() != YAML::NodeType::Null)
    {
      project.name = m_node["project"]["name"].as<std::string>();
    }

  if (m_node["project"]["authors"] && m_node["project"]["authors"].Type() != YAML::NodeType::Null)
    {
      for (size_t i = 0; i<m_node["project"]["authors"].size(); i++)
	{
	  project.authors.push_back( m_node["project"]["authors"][i].as<std::string>() );
	}
    }

  if (m_node["project"]["license"] && m_node["project"]["license"].Type() != YAML::NodeType::Null)
    {
      project.license = m_node["project"]["license"].as<std::string>();
    }

  if (m_node["project"]["version"] && m_node["project"]["version"].Type() != YAML::NodeType::Null)
    {
      project.version = m_node["project"]["version"].as<std::string>();
    }
  return project;
}

Install YamlReader::readInstall() const
{
  Install install;
  
  if (!m_node["install"])
    {
      return install;
    }  

  if (m_node["install"]["bin-dir"] && m_node["install"]["bin-dir"].Type() != YAML::NodeType::Null)
    {
      install.bin_dir = m_node["install"]["bin-dir"].as<std::string>();
    }

  if (m_node["install"]["asset-dir"] && m_node["install"]["asset-dir"].Type() != YAML::NodeType::Null)
    {
      install.asset_dir = m_node["install"]["asset-dir"].as<std::string>();
    }
  
  return install;
}

std::vector<Asset> YamlReader::readAssets() const
{
  std::vector<Asset> assets;  

  if (!m_node["assets"])
    {
      return assets;
    }
  
  for (size_t i=0; i<m_node["assets"].size(); i++)
    {
      Asset asset;

      for (size_t j=0; j<m_node["assets"][i]["asset"]["dirs"].size(); j++)
      	{
      	  asset.dirs.push_back(m_node["assets"][i]["asset"]["dirs"][j].as<std::string>());
      	}

      assets.push_back(asset);
    }

  return assets;
}

Common YamlReader::readCommon() const
{
  Common common;

  if (!m_node["common"])
    {
      return common;
    }

  if (m_node["common"]["sources"] && m_node["common"]["sources"].Type() != YAML::NodeType::Null)
    {
      for (size_t i=0; i<m_node["common"]["sources"].size(); i++)
	{
	  common.sources.push_back( m_node["common"]["sources"][i].as<std::string>() );
	}
    }

  if (m_node["common"]["includes"] && m_node["common"]["includes"].Type() != YAML::NodeType::Null)
    {
      for (size_t i=0; i<m_node["common"]["includes"].size(); i++)
	{
	  common.includes.push_back( m_node["common"]["includes"][i].as<std::string>() );
	}
    }

  if (m_node["common"]["flags"] && m_node["common"]["flags"].Type() != YAML::NodeType::Null)
    {
      for (size_t i=0; i<m_node["common"]["flags"].size(); i++)
	{
	  common.flags.push_back( m_node["common"]["flags"][i].as<std::string>() );
	}
    }

  if (m_node["common"]["libs"] && m_node["common"]["libs"].Type() != YAML::NodeType::Null)
    {
      for (size_t i=0; i<m_node["common"]["libs"].size(); i++)
	{
	  common.libs.push_back( m_node["common"]["libs"][i].as<std::string>() );
	}
    }

  if (m_node["common"]["obj-dir"] && m_node["common"]["obj-dir"].Type() != YAML::NodeType::Null)
    {
      common.obj_dir = m_node["common"]["obj-dir"].as<std::string>();
    }
  
  return common;
}

std::map<std::string, Target> YamlReader::readTargets() const
{
  std::map<std::string, Target> targets;

  if (!m_node["targets"])
    {
      return targets;
    }
  
  for (size_t i=0; i<m_node["targets"].size(); i++)
    {
      Target target;
      
      if (m_node["targets"][i]["target"]["name"] &&
	  m_node["targets"][i]["target"]["name"].Type() != YAML::NodeType::Null)
      	{
      	  target.name = m_node["targets"][i]["target"]["name"].as<std::string>();
      	}

      if (m_node["targets"][i]["target"]["toolchain-prefix"] &&
	  m_node["targets"][i]["target"]["toolchain-prefix"].Type() != YAML::NodeType::Null)
      	{
      	  target.toolchain_prefix = m_node["targets"][i]["target"]["toolchain-prefix"].as<std::string>();
      	}

      if (m_node["targets"][i]["target"]["exec-path"] &&
	  m_node["targets"][i]["target"]["exec-path"].Type() != YAML::NodeType::Null)
      	{
      	  target.exec_path = m_node["targets"][i]["target"]["exec-path"].as<std::string>();
      	}

      if (m_node["targets"][i]["target"]["obj-dir"] &&
	  m_node["targets"][i]["target"]["obj-dir"].Type() != YAML::NodeType::Null)
      	{
      	  target.obj_dir = m_node["targets"][i]["target"]["obj-dir"].as<std::string>();
      	}

      if (m_node["targets"][i]["target"]["sources"] &&
	  m_node["targets"][i]["target"]["sources"].Type() != YAML::NodeType::Null)
	{
	  for (size_t j=0; j<m_node["targets"][i]["target"]["sources"].size(); j++)
	    {
	      target.sources.push_back( m_node["targets"][i]["target"]["sources"][j].as<std::string>() );
	    }
	}
      
      if (m_node["targets"][i]["target"]["includes"] &&
	  m_node["targets"][i]["target"]["includes"].Type() != YAML::NodeType::Null)
	{
	  for (size_t j=0; j<m_node["targets"][i]["target"]["includes"].size(); j++)
	    {
	      target.includes.push_back( m_node["targets"][i]["target"]["includes"][j].as<std::string>() );
	    }
	}

      if (m_node["targets"][i]["target"]["libs"] &&
	  m_node["targets"][i]["target"]["libs"].Type() != YAML::NodeType::Null)
	{
	  for (size_t j=0; j<m_node["targets"][i]["target"]["libs"].size(); j++)
	    {
	      target.libs.push_back( m_node["targets"][i]["target"]["libs"][j].as<std::string>() );
	    }
	}

      if (m_node["targets"][i]["target"]["flags"] &&
	  m_node["targets"][i]["target"]["flags"].Type() != YAML::NodeType::Null)
	{
	  for (size_t j=0; j<m_node["targets"][i]["target"]["flags"].size(); j++)
	    {
	      target.flags.push_back( m_node["targets"][i]["target"]["flags"][j].as<std::string>() );
	    }
	}
      targets.insert({target.name, target});
    }
  

  return targets;
}
  
YamlReader::~YamlReader()
{
  
}
