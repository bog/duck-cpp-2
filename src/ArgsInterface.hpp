#ifndef ARGSINTERFACE_HPP
#define ARGSINTERFACE_HPP
#include <iostream>

class ArgsParser;
class ProjectFactory;
class FileCreator;
class FileChecker;
class FileReader;

class ArgsInterface
{
public:
  explicit ArgsInterface(ArgsParser& args_parser,
			 ProjectFactory& project_factory,
			 FileCreator& file_creator,
			 FileChecker& file_checker,
			 FileReader& file_reader);
  void execute();
  virtual ~ArgsInterface();
  
protected:
  
private:
  ArgsParser& m_args_parser;
  ProjectFactory& m_project_factory;
  FileCreator& m_file_creator;
  FileChecker& m_file_checker;
  FileReader& m_file_reader;
  ArgsInterface( ArgsInterface const& argsinterface ) = delete;
  ArgsInterface& operator=( ArgsInterface const& argsinterface ) = delete;
};

#endif
