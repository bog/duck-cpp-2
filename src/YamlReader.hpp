#ifndef YAMLREADER_HPP
#define YAMLREADER_HPP
#include <iostream>
#include <map>
#include <vector>
#include <yaml-cpp/yaml.h>
#include "Sections.hpp"

class YamlReader
{
public:
  explicit YamlReader(std::string const& sources);
  
  Project readProject() const;
  Install readInstall() const;
  std::vector<Asset> readAssets() const;
  Common readCommon() const;
  std::map<std::string, Target> readTargets() const;
  
  virtual ~YamlReader();
  
protected:
  
private:
  YAML::Node m_node;
  
  YamlReader( YamlReader const& yamlreader ) = delete;
  YamlReader& operator=( YamlReader const& yamlreader ) = delete;
};

#endif
