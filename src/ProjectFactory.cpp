#include <sstream>
#include <ProjectFactory.hpp>
#include "FileCreator.hpp"
#include "FileChecker.hpp"
#include "YamlFactory.hpp"

ProjectFactory::ProjectFactory(FileCreator& file_creator,
			       FileChecker& file_checker,
			       YamlFactory& yaml_factory)
  : m_file_creator { file_creator }
  , m_file_checker { file_checker }
  , m_yaml_factory { yaml_factory }
{

}

void ProjectFactory::initSimple(std::string const& name)
{
  if (m_file_checker.exists("build"))
    {
      throw std::runtime_error("The directory build already exists.");
    }
  if (m_file_checker.exists("build/bin"))
    {
      throw std::runtime_error("The directory build/bin already exists.");
    }

  if (m_file_checker.exists("build/obj"))
    {
      throw std::runtime_error("The directory build/obj already exists.");
    }

  if (m_file_checker.exists("src"))
    {
      throw std::runtime_error("The directory src already exists.");
    }

  m_file_creator.createDir("build");
  m_file_creator.createDir("build/bin");
  m_file_creator.createDir("build/obj");
  m_file_creator.createDir("src");
  
  std::stringstream source;
  source << "#include <iostream>" << std::endl;
  source << "" << std::endl;
  source << "" << std::endl;
  source << "int main(int argc, char** argv)" << std::endl;
  source << "{" << std::endl;
  source << "\t" << "std::cout << \""<<name<<"\" << \" !\" << std::endl;" << std::endl;
  source << "\t" << "return 0;" << std::endl;
  source << "}" << std::endl;
  
  if ( !m_file_checker.exists("src/" + name + ".cpp") )
    {
      m_file_creator.createFile("src/" + name + ".cpp", source.str());
    }
  else
    {
      throw std::runtime_error(name + ".cpp already exist.");
    }

  if ( !m_file_checker.exists("duck.yml") )
    {
      m_file_creator.createFile("duck.yml", m_yaml_factory.simple(name));
    }
  else
    {
      throw std::runtime_error("duck.yml already exist.");
    }
}

ProjectFactory::~ProjectFactory()
{
  
}
