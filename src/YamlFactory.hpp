#ifndef YAMLFACTORY_HPP
#define YAMLFACTORY_HPP
#include <iostream>

class YamlFactory
{
public:
  explicit YamlFactory();
  std::string simple(std::string const& project_name);
  
  virtual ~YamlFactory();
  
protected:
  
private:
  YamlFactory( YamlFactory const& yamlfactory ) = delete;
  YamlFactory& operator=( YamlFactory const& yamlfactory ) = delete;
};

#endif
