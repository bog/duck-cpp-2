#ifndef ARGSPARSER_HPP
#define ARGSPARSER_HPP
#include <iostream>
#include <vector>

class ArgsParser
{
public:
  explicit ArgsParser(std::vector<std::string> args);
  std::string getAction() const;
  std::vector<std::string> getParams() const;
  size_t size() const;
  virtual ~ArgsParser();
  
protected:
  
private:
  std::vector<std::string> m_args;
  
  ArgsParser( ArgsParser const& argsparser ) = delete;
  ArgsParser& operator=( ArgsParser const& argsparser ) = delete;
};

#endif
