#ifndef FILECREATOR_HPP
#define FILECREATOR_HPP
#include <iostream>

class FileCreator
{
public:
  explicit FileCreator();

  virtual void createDir(std::string const& name);
  virtual void createFile(std::string const& name, std::string const& content);
  virtual void remove(std::string const& name);
  
  virtual ~FileCreator();
  
protected:
  
private:
  FileCreator( FileCreator const& filecreator ) = delete;
  FileCreator& operator=( FileCreator const& filecreator ) = delete;
};

#endif
