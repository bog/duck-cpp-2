#ifndef MAKEFILECHECKER_HPP
#define MAKEFILECHECKER_HPP
#include <iostream>

class FileChecker;
class MakefileBuilder;

class MakefileChecker
{
public:
  explicit MakefileChecker(FileChecker& file_checker, MakefileBuilder& builder);
  bool validate();
  std::vector<std::string> errors() const;
  
  virtual ~MakefileChecker();
  
protected:
  
private:
  FileChecker& m_file_checker;
  MakefileBuilder& m_builder;
  std::vector<std::string> m_errors;
  
  MakefileChecker( MakefileChecker const& makefilechecker ) = delete;
  MakefileChecker& operator=( MakefileChecker const& makefilechecker ) = delete;
};

#endif
