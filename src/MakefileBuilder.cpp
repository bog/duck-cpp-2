#include <filesystem>
#include <sstream>
#include <regex>
#include <algorithm>
#include <MakefileBuilder.hpp>
#include "FileChecker.hpp"
#include <YamlReader.hpp>

MakefileBuilder::MakefileBuilder()
{
  m_project.name = "new-project";
  m_project.license = "MIT";
  m_project.version = "0";

  m_install.is_init = false;
  m_install.bin_dir = "/usr/local/bin";
  m_install.asset_dir = "/usr/local/share/new-project";
  m_common.obj_dir = ".";
}

MakefileBuilder& MakefileBuilder::commonSources(std::vector<std::string> const& sources)
{
  for (std::string const& value : sources)
    {
      m_common.sources.push_back(removeEndSlashes(value));
    }
  
  return *this;
}

MakefileBuilder& MakefileBuilder::commonIncludes(std::vector<std::string> const& includes)
{
  for (std::string const& value : includes)
    {
      m_common.includes.push_back(removeEndSlashes(value));
    }
  
  return *this;
}

MakefileBuilder& MakefileBuilder::commonFlags(std::vector<std::string> const& flags)
{
  for (std::string const& value : flags)
    {
      m_common.flags.push_back(removeEndSlashes(value));
    }

  return *this;
}

MakefileBuilder& MakefileBuilder::commonLibs(std::vector<std::string> const& libs, FileChecker const& checker)
{  
  std::vector<std::string> tags, pkgs, files, dirs;
  
  for (std::string const& value : libs)
    {
      std::string v = removeEndSlashes(value);
      
      if (!v.empty() && v[0] == '-')
	{
	  tags.push_back(v);
	}
      else if (checker.isDir(v))
	{
	  dirs.push_back("-L " + v);
	}
      else if (checker.isFile(v))
	{
	  files.push_back("-l " + v);
	}
      else
	{
	  pkgs.push_back("$(shell pkg-config --libs --cflags " + v + ")");
	}
    }

  for (std::string const& lib : pkgs) { m_common.libs.push_back(lib); }
  for (std::string const& lib : dirs) { m_common.libs.push_back(lib); }
  for (std::string const& lib : files) { m_common.libs.push_back(lib); }
  for (std::string const& lib : tags) { m_common.libs.push_back(lib); }

  return *this;
}

MakefileBuilder& MakefileBuilder::commonObjDir(std::string const& obj_dir)
{
  if (obj_dir.empty())
    {
      m_common.obj_dir = ".";
    }
  else
    {
      m_common.obj_dir = removeEndSlashes(obj_dir);
    }
  
  return *this;
}

MakefileBuilder& MakefileBuilder::targetToolchainPrefix(std::string const& name, std::string const& toolchain_prefix)
{
  m_targets[name].toolchain_prefix = toolchain_prefix;
  m_targets[name].name = name;
  return *this;
}

MakefileBuilder& MakefileBuilder::targetExecPath(std::string const& name, std::string const& exec_path)
{
  m_targets[name].exec_path = exec_path;
  m_targets[name].name = name;
  
  return *this;
}

MakefileBuilder& MakefileBuilder::targetSources(std::string const& name, std::vector<std::string> sources)
{
  for (std::string const& value : sources)
    {
      m_targets[name].sources.push_back(removeEndSlashes(value));
    }

  m_targets[name].name = name;
  
  return *this;
}

MakefileBuilder& MakefileBuilder::targetIncludes(std::string const& name, std::vector<std::string> includes)
{

  for (std::string const& value : includes)
    {
      m_targets[name].includes.push_back(removeEndSlashes(value));
    }

  m_targets[name].name = name;
  
  return *this;
}

MakefileBuilder& MakefileBuilder::targetLibs(std::string const& name,
					     std::vector<std::string> libs,
					     FileChecker const& checker)
{
  std::vector<std::string> tags, pkgs, files, dirs;
  
  for (std::string const& value : libs)
    {
      std::string v = removeEndSlashes(value);
      
      if (!v.empty() && v[0] == '-')
	{
	  tags.push_back(v);
	}
      else if (checker.isDir(v))
	{
	  dirs.push_back("-L " + v);
	}
      else if (checker.isFile(v))
	{
	  files.push_back("-l " + v);
	}
      else
	{
	  pkgs.push_back("$(shell pkg-config --libs --cflags " + v + ")");
	}
    }

  for (std::string const& lib : pkgs) { m_targets[name].libs.push_back(lib); }
  for (std::string const& lib : dirs) { m_targets[name].libs.push_back(lib); }
  for (std::string const& lib : files) { m_targets[name].libs.push_back(lib); }
  for (std::string const& lib : tags) { m_targets[name].libs.push_back(lib); }
  
  m_targets[name].name = name;
  
  return *this;
}

MakefileBuilder& MakefileBuilder::targetFlags(std::string const& name, std::vector<std::string> flags)
{
  for (std::string const& value : flags)
    {
      m_targets[name].flags.push_back(removeEndSlashes(value));
    }

  m_targets[name].name = name;
  
  return *this;
}

MakefileBuilder& MakefileBuilder::targetObjDir(std::string const& name, std::string obj_dir)
{
  if (obj_dir.empty())
    {
      m_targets[name].obj_dir = ".";
    }
  else
    {
      m_targets[name].obj_dir = removeEndSlashes(obj_dir);
    }

  m_targets[name].name = name;
  
  return *this;
}

MakefileBuilder& MakefileBuilder::assetDirs(std::vector<std::string> dirs)
{
  Asset asset;
  
  for (std::string const& value : dirs)
    {
      asset.dirs.push_back(removeEndSlashes(value));
    }

  m_assets.push_back(asset);
  return *this;
}

MakefileBuilder& MakefileBuilder::projectName(std::string const& name)
{
  if ( !name.empty() )
    {
      m_project.name = name;

      if (!m_install.is_init)
	{
	  m_install.asset_dir = "/usr/local/share/" + m_project.name;
	}
    }
  
  return *this;
}

MakefileBuilder& MakefileBuilder::projectAuthors(std::vector<std::string> authors)
{
  if ( !authors.empty() )
    {
      m_project.authors = authors;
    }
  
  return *this;
}

MakefileBuilder& MakefileBuilder::projectLicense(std::string const& license)
{
  if ( !license.empty() )
    {
      m_project.license = license;
    }
  
  return *this;
}

MakefileBuilder& MakefileBuilder::projectVersion(std::string const& version)
{
  if ( !version.empty() )
    {
      m_project.version = version;
    }
  
  return *this;
}

MakefileBuilder& MakefileBuilder::installBinDir(std::string const& bin_dir)
{
  if (!bin_dir.empty())
    {
      m_install.bin_dir = removeEndSlashes(bin_dir);
    }
  
  return *this;
}

MakefileBuilder& MakefileBuilder::installAssetDir(std::string const& asset_dir)
{
  if (!asset_dir.empty())
    {
      m_install.asset_dir = removeEndSlashes(asset_dir);
      m_install.is_init = true;
    }

  return *this;
}

MakefileBuilder& MakefileBuilder::yamlReader(YamlReader const& yaml, FileChecker const& checker)
{
  Project project = yaml.readProject();
  Install install = yaml.readInstall();
  std::vector<Asset> assets = yaml.readAssets();
  Common common = yaml.readCommon();
  std::map<std::string, Target> targets = yaml.readTargets();
  
  projectName(project.name);
  projectAuthors(project.authors);
  projectLicense(project.license);
  projectVersion(project.version);

  installBinDir(install.bin_dir);
  installAssetDir(install.asset_dir);

  for (Asset const& asset : assets)
    {
      assetDirs(asset.dirs);
    }

  commonSources(common.sources);
  commonIncludes(common.includes);
  commonFlags(common.flags);
  commonLibs(common.libs, checker);
  commonObjDir(common.obj_dir);

  for (auto const& p : targets)
    {
      Target target = p.second;
      targetToolchainPrefix(target.name, target.toolchain_prefix);
      targetExecPath(target.name, target.exec_path);
      targetSources(target.name, target.sources);
      targetIncludes(target.name, target.includes);
      targetFlags(target.name, target.flags);
      targetLibs(target.name, target.libs, checker);
      targetObjDir(target.name, target.obj_dir);  
    }
   
  return *this;
}

std::string MakefileBuilder::build()
{
  std::stringstream output;
  
  /////////////
  // COMMONS //
  /////////////

  /* CXXFLAGS */  
  output << "CXXFLAGS=";
  
  if (m_common.includes.empty())
    {
      output << " -I ./";
    }
  
  for (size_t i=0; i<m_common.includes.size(); i++)
    {
      if (std::filesystem::path(m_common.includes[i]).extension() == "")
	{
	  output << " -I " << m_common.includes[i];
	}
    }
  for (size_t i=0; i<m_common.includes.size(); i++)
    {
      if (std::filesystem::path(m_common.includes[i]).extension() != "")
	{
	  output << " -I " << m_common.includes[i];
	}
    }
  
  for (size_t i=0; i<m_common.flags.size(); i++)
    {
      if (std::filesystem::path(m_common.flags[i]).extension() == "")
	{
	  output << " " << m_common.flags[i];
	}
    }  

  for (size_t i=0; i<m_common.flags.size(); i++)
    {
      if (std::filesystem::path(m_common.flags[i]).extension() != "")
	{
	  output << " " << m_common.flags[i];
	}
    }
  output << std::endl;
  
  /* LDFLAGS */
  output << "LDFLAGS=";
  for (size_t i=0; i<m_common.libs.size(); i++)
    {
      output << " " << m_common.libs[i];
    }
  output << std::endl;
  
  /* SRC */
  output << "SRC=";
  if (m_common.sources.empty())
    {
      output << " $(wildcard *.cpp)";
    }
  
  for (size_t i=0; i<m_common.sources.size(); i++)
    {
      if (std::filesystem::path(m_common.sources[i]).extension() == "")
	{
	  output << " $(wildcard " << m_common.sources[i] << "/*.cpp)";
	}
    }
    
  for (size_t i=0; i<m_common.sources.size(); i++)
    {
      if (std::filesystem::path(m_common.sources[i]).extension() != "")
	{
	  output << " " << m_common.sources[i];
	}
    }     
  output << std::endl;

  /* OBJ */
  output << "OBJDIR= " << m_common.obj_dir << std::endl;
  output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  /////////////
  // TARGETS //
  /////////////

  if (m_targets.empty())
    {
      m_targets["gnu64"].name = "gnu64";
    }
  
  for (std::pair<std::string, Target> p : m_targets)
    {
      std::string name = p.first;
      Target target = p.second;

      if (name.empty())
	{
	  name = "gnu64";
	}
      if (target.exec_path.empty())
	{
	  target.exec_path = "a.out";
	}
      if (target.obj_dir.empty())
	{
	  target.obj_dir = ".";
	}
      output << name << ": " <<target.exec_path<< std::endl;
      output << "CXXFLAGS_"<<name<<"=";
      for (auto const& inc : target.includes)
	{
	  if (std::filesystem::path(inc).extension() == "")
	    {
	      output << " -I " << inc;
	    }
	}
      for (auto const& inc : target.includes)
	{
	  if (std::filesystem::path(inc).extension() != "")
	    {
	      output << " -I " << inc;
	    }
	}
      
      for (auto const& flag : target.flags)
	{
	  output << " " << flag;
	}      
      output << std::endl;
      
      output << "LDFLAGS_"<<name<<"=";
      
      for (auto const& lib : target.libs)
	{
	  output<< " " << lib;
	}

      output << std::endl;
      
      output << "SRC_"<<name<<"=";
      for (auto const& src : target.sources)
	{
	  if (std::filesystem::path(src).extension() == "")
	    {
	      output << " $(wildcard " << src << "/*.cpp)";
	    }
	}
      for (auto const& src : target.sources)
	{
	  if (std::filesystem::path(src).extension() != "")
	    {
	      output << " " << src;
	    }
	}
      output << std::endl;
	    
      output << "OBJDIR_"<<name<<"= " <<target.obj_dir<< std::endl;      
      output << "OBJ_"<<name
	     <<"= $(foreach src, $(SRC_"<<name
	     <<"), $(OBJDIR_"<<name
	     <<")/$(notdir $(src:%.cpp=%.o)))" << std::endl;

      output << "run-" << target.name << ": " << target.exec_path << std::endl;
      output << "\t" << "./$^" << std::endl;

      std::string includes = "";

      for (std::string const& include : target.includes)
	{
	  if (std::filesystem::path(include).extension() == "")
	    {
	      includes += " $(wildcard " + include + "/*.hpp)";
	    }
	}

      for (std::string const& include : target.includes)
	{
	  if (std::filesystem::path(include).extension() != "")
	    {
	      includes += " " + include;
	    }
	}
	    
      output << target.exec_path << ":$(OBJ) $(OBJ_"<<name<<")"<<includes << std::endl;
      
      if (std::filesystem::path(target.exec_path).extension() == ".a")
	// static lib
	{
	  output << "\t" << "ar rvs $@ $^" << std::endl;
	}
      else // executable
	{
      
	  if (target.toolchain_prefix.empty())
	    {
	      output << "\t"
		     << "$(CXX) $(CXXFLAGS) $(CXXFLAGS_"
		     <<name
		     <<") $(OBJ) $(OBJ_"<<name<<") $(LDFLAGS) $(LDFLAGS_"<<name<<") -o $@" << std::endl;
	    }
	  else
	    {
	      output << "\t" << target.toolchain_prefix
		     <<"-$(CXX) $(CXXFLAGS) $(CXXFLAGS_"
		     <<name
		     <<") $(OBJ) $(OBJ_"<<name<<") $(LDFLAGS) $(LDFLAGS_"<<name<<") -o $@" << std::endl;
	    }      

	}
      
      //////////////
      // INSTALL //
      /////////////
      std::string exec = target.exec_path;
      exec = std::filesystem::path(exec).filename();
      
      output << "install-" << target.name << ": " << target.exec_path << std::endl;
      output << "\t" << "cp -vr $^ " << m_install.bin_dir << std::endl;      

      for (Asset const& asset : m_assets)
	{
	  output << "\t" << "cp -vr";
	  for (std::string const& dir : asset.dirs)
	    {
	      output << " " << dir;
	    }
	  output << " " << m_install.asset_dir << std::endl;      
	}
      
      output << "uninstall-" << target.name << ":" << std::endl;
      output << "\t" << "rm -vir " << m_install.bin_dir << "/" << exec << std::endl;
      for (Asset const& asset : m_assets)
	{
	  output << "\t" << "rm -vir";
	  for (std::string const& dir : asset.dirs)
	    {
	      output << " " << m_install.asset_dir << "/" << dir;
	    }
	  output << std::endl;
	}
    }
  
  /////////////
  // OBJ GEN //
  /////////////
  std::vector<std::string> paths;
  
  if (m_common.sources.empty())
    {
      output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;
    }
  
  bool find_empty = false;
  
  for (std::string const& source : m_common.sources)
    {
      std::string src = source;

      if (std::filesystem::path(src).extension() != "")
	{
	  src = std::filesystem::path(src).parent_path();
	}
      
      
      if (src.empty() && !find_empty)
	{
	  find_empty = true;
	  output << "$(OBJDIR)/%.o:%.cpp" << std::endl;

	  output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;
	}
      else if( !src.empty() )
	{
	  output << "$(OBJDIR)/%.o:"<< src<<"/%.cpp"<< std::endl;
	  output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;
	}
      
      paths.push_back(src);
    }

  for (std::pair<std::string, Target> p: m_targets)
    {
      Target target = p.second;

      std::string toolchain = "";
      
      if ( !target.toolchain_prefix.empty() )
	{
	  toolchain = target.toolchain_prefix + "-";
	}
      
      if (target.sources.empty())
	{
	  output << "$(OBJDIR_"<<target.name<<")/%.o:%.cpp $(wildcard *.hpp)" << std::endl;

	  output << "\t" <<toolchain<< "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_"
		 <<target.name
		 <<") $< $(LDFLAGS) $(LDFLAGS_"
		 <<target.name<<") -o $@" << std::endl;

	}

      find_empty = false;
      
      for (std::string const& source : target.sources)
	{
	  std::string src = source;

	  if (std::filesystem::path(src).extension() != "")
	    {
	      src = std::filesystem::path(src).parent_path();
	    }

	  if (src.empty() && !find_empty)
	    {
	      find_empty = true;
	      output << "$(OBJDIR_"<<target.name<<")/%.o:%.cpp $(wildcard *.hpp)" << std::endl;

	      output << "\t" << toolchain << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_"
		     <<target.name
		     <<") $< $(LDFLAGS) $(LDFLAGS_"
		     <<target.name<<") -o $@" << std::endl;
	    }
	  else if ( !src.empty() )
	    {
	     
	      output << "$(OBJDIR_"<<target.name<<")/%.o:"<< src<<"/%.cpp" << " $(wildcard " << src << "/*.hpp)" << std::endl;

	      output << "\t" << toolchain << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_"
		     <<target.name
		     <<") $< $(LDFLAGS) $(LDFLAGS_"
		     <<target.name<<") -o $@" << std::endl;
	    }

	}
    }

  ///////////
  // CHECK //
  ///////////

  output << "check:" << std::endl;
  output << "\t";  
  output << "cppcheck --enable=all $(SRC)";  
  for (auto const& p : m_targets)
    {
      Target target = p.second;
      output << " " << "$(SRC_" << target.name << ")";
    }
  output << "" << std::endl;

  ////////////
  // CLEAN //
  ///////////

  output << "clean:" << std::endl;
  output << "\t";  
  output << "rm -f $(OBJ)/*.o";  
  for (auto const& p : m_targets)
    {
      Target target = p.second;
      output << " " << "$(OBJDIR_" << target.name << ")/*.o";
    }
  output << "" << std::endl;

  ///////////////
  // MRPROPER //
  ///////////////

  output << "mrproper: clean" << std::endl;
  output << "\trm -f ";
  
  for (auto const& p : m_targets)
    {
      Target target = p.second;
      if ( !target.exec_path.empty() )
	{
	  output << " " << target.exec_path;
	}
      else
	{
	  output << " a.out";
	}
      
    }
  
  output << std::endl;

  ////////////
  // PHONY //
  ///////////

  output << ".PHONY: ";
  for (auto const& p : m_targets)
    {
      Target target = p.second;
      output << " " << target.name << " "
	     << "run-" << target.name << " "
	     << "install-" << target.name << " "
	     << "uninstall-" << target.name;
    }

  output << " check clean mrproper" << std::endl;
  
  return uniqueSpace(output.str());
}

std::vector<Asset> MakefileBuilder::getAssets() const
{
  return m_assets;
}

Project MakefileBuilder::getProject() const
{
  return m_project;
}

Install MakefileBuilder::getInstall() const
{
  return m_install;
}

Common MakefileBuilder::getCommon() const
{
  return m_common;
}

std::map<std::string, Target> MakefileBuilder::getTargets() const
{
  return m_targets;
}

std::string MakefileBuilder::uniqueSpace(std::string const& str)
{
  std::string res = "";

  for (size_t i=0; i<str.size(); i++)
    {
      if (i > 0 && str[i-1] == ' ' && str[i] == ' ') { continue; }
      else
	{
	  res += str[i];
	}
    }
  
  return res;
}

std::string MakefileBuilder::removeEndSlashes(std::string const& str)
{
  std::string res = std::regex_replace(str, std::regex("//"), "/");   
  res = std::regex_replace(res, std::regex("//"), "/");

  if (res[res.size() - 1] ==  '/') { return res.substr(0, res.size() - 1); }
  return res;
}

MakefileBuilder::~MakefileBuilder()
{
  
}
