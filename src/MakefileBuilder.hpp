#ifndef MAKEFILEBUILDER_HPP
#define MAKEFILEBUILDER_HPP
#include <iostream>
#include <vector>
#include <unordered_map>
#include <map>
#include "Sections.hpp"

class YamlReader;
class FileChecker;

class MakefileBuilder
{
public:
  explicit MakefileBuilder();
  MakefileBuilder& commonSources(std::vector<std::string> const& sources);
  MakefileBuilder& commonIncludes(std::vector<std::string> const& includes);
  MakefileBuilder& commonFlags(std::vector<std::string> const& flags);
  MakefileBuilder& commonLibs(std::vector<std::string> const& libs, FileChecker const& checker);
  MakefileBuilder& commonObjDir(std::string const& obj_dir);

  MakefileBuilder& targetToolchainPrefix(std::string const& name, std::string const& toolchain_prefix);
  MakefileBuilder& targetExecPath(std::string const& name, std::string const& exec_path);
  MakefileBuilder& targetSources(std::string const& name, std::vector<std::string> sources);
  MakefileBuilder& targetIncludes(std::string const& name, std::vector<std::string> includes);
  MakefileBuilder& targetLibs(std::string const& name, std::vector<std::string> libs, FileChecker const& checker);
  MakefileBuilder& targetFlags(std::string const& name, std::vector<std::string> flags);
  MakefileBuilder& targetObjDir(std::string const& name, std::string obj_dir);
  
  MakefileBuilder& assetDirs(std::vector<std::string> dirs);
  
  MakefileBuilder& projectName(std::string const& name);
  MakefileBuilder& projectAuthors(std::vector<std::string> authors);
  MakefileBuilder& projectLicense(std::string const& license);
  MakefileBuilder& projectVersion(std::string const& version);
  
  MakefileBuilder& installBinDir(std::string const& bin_dir);
  MakefileBuilder& installAssetDir(std::string const& asset_dir);
  MakefileBuilder& yamlReader(YamlReader const& yaml, FileChecker const& checker);
  
  std::string build();

  std::vector<Asset> getAssets() const;
  Project getProject() const;
  Install getInstall() const;
  Common getCommon() const;
  std::map<std::string, Target> getTargets() const;
  
  virtual ~MakefileBuilder();
  
protected:
  
private:
  Common m_common;
  std::map<std::string, Target> m_targets;
  std::vector<Asset> m_assets;
  Project m_project;
  Install m_install;

  std::string uniqueSpace(std::string const& str);
  std::string removeEndSlashes(std::string const& str);
  
  MakefileBuilder( MakefileBuilder const& makefilebuilder ) = delete;
  MakefileBuilder& operator=( MakefileBuilder const& makefilebuilder ) = delete;
};

#endif
