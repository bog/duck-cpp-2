#ifndef FILECHECKER_HPP
#define FILECHECKER_HPP
#include <iostream>

class FileChecker
{
public:
  explicit FileChecker();
  virtual bool exists(std::string const& dir) const;
  virtual bool isFile(std::string const& lib) const;
  virtual bool isDir(std::string const& dir) const;
  virtual ~FileChecker();
  
protected:
  
private:
  FileChecker( FileChecker const& filechecker ) = delete;
  FileChecker& operator=( FileChecker const& filechecker ) = delete;
};

#endif
