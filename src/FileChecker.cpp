#include <filesystem>
#include <FileChecker.hpp>

FileChecker::FileChecker()
{
  
}

/*virtual*/ bool FileChecker::exists(std::string const& dir) const
{  
  return std::filesystem::exists(std::filesystem::path {dir});
}

/*virtual*/ bool FileChecker::isFile(std::string const& file) const
{
  return std::filesystem::is_regular_file( std::filesystem::path(file) );
}

/*virtual*/ bool FileChecker::isDir(std::string const& dir) const
{
  return std::filesystem::is_directory( std::filesystem::path(dir) );
}

FileChecker::~FileChecker()
{
  
}
