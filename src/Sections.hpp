#ifndef SECTIONS_HPP
#define SECTIONS_HPP
#include <iostream>
#include <vector>

struct Common
{
  std::vector<std::string> sources;
  std::vector<std::string> includes;
  std::vector<std::string> flags;
  std::vector<std::string> libs;
  std::string obj_dir;
};

struct Target
{
  std::string name;
  std::string toolchain_prefix;
  std::string exec_path;
  std::vector<std::string> sources;
  std::vector<std::string> includes;
  std::vector<std::string> libs;
  std::vector<std::string> flags;
  std::string obj_dir;
};

struct Asset
{
  std::vector<std::string> dirs;
};

struct Project
{
  std::string name;
  std::vector<std::string> authors;
  std::string license;
  std::string version;
};

struct Install
{
  bool is_init;
  std::string bin_dir;
  std::string asset_dir;
};

#endif
