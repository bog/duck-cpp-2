#include <iostream>
#include <vector>
#include "ArgsInterface.hpp"
#include "ArgsParser.hpp"
#include "FileCreator.hpp"
#include "FileChecker.hpp"
#include "FileReader.hpp"
#include "YamlFactory.hpp"
#include "ProjectFactory.hpp"

#include "ArgsInterface.hpp"

int main(int argc, char** argv)
{
  std::vector<std::string> args;

  for (int i=1; i<argc; i++)
    {
      args.push_back(argv[i]);
    }
  
  ArgsParser args_parser(args);
  FileCreator file_creator;
  FileChecker file_checker;
  FileReader file_reader;
  YamlFactory yaml_factory;
  ProjectFactory project(file_creator, file_checker, yaml_factory);
  
  ArgsInterface interface { args_parser, project, file_creator, file_checker, file_reader };

  try
    {
      interface.execute();
    }
  catch(std::exception const& e)
    {
      std::cerr<<"E: " << e.what() <<std::endl;
      exit(-1);
    }
  
  return 0;
}
