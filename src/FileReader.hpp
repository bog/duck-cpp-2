#ifndef FILEREADER_HPP
#define FILEREADER_HPP
#include <iostream>

class FileReader
{
public:
  explicit FileReader();
  virtual std::string from(std::string const& path) const;
  
  virtual ~FileReader();
  
protected:
  
private:
  FileReader( FileReader const& filereader ) = delete;
  FileReader& operator=( FileReader const& filereader ) = delete;
};

#endif
