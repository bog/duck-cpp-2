#ifndef PROJECTFACTORY_HPP
#define PROJECTFACTORY_HPP
#include <iostream>

class FileCreator;
class FileChecker;
class YamlFactory;
class ProjectFactory
{
public:
  ProjectFactory(FileCreator& file_creator,
		 FileChecker& file_checker,
		 YamlFactory& yaml_factory);
  
  void initSimple(std::string const& name);
  virtual ~ProjectFactory();
  
protected:

private:
  FileCreator& m_file_creator;
  FileChecker& m_file_checker;
  YamlFactory& m_yaml_factory;
  
  ProjectFactory( ProjectFactory const& projectfactory ) = delete;
  ProjectFactory& operator=( ProjectFactory const& projectfactory ) = delete;
};

#endif
