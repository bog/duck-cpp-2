#include <fstream>
#include <filesystem>
#include "FileCreator.hpp"

FileCreator::FileCreator()
{
  
}

/*virtual*/ void FileCreator::createDir(std::string const& name)
{
  std::filesystem::create_directory(name);
}

/*virtual*/ void FileCreator::createFile(std::string const& name, std::string const& content)
{
  if ( std::filesystem::exists(name) )
    {
      throw std::runtime_error("Directory " + name + " already exist.");
    }

  std::ofstream file {name};

  if (!file)
    {
      throw std::runtime_error("Cannot create " + name + ".");
    }

  file << content;
}


/*virtual*/ void FileCreator::remove(std::string const& name)
{
  std::filesystem::remove_all( std::filesystem::path(name) );
}


FileCreator::~FileCreator()
{
  
}
