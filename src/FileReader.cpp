#include <fstream>
#include <FileReader.hpp>

FileReader::FileReader()
{
  
}

/*virtual*/ std::string FileReader::from(std::string const& path) const
{
  std::ifstream file { path };

  if (!file)
    {
      throw std::runtime_error("Cannot find " + path + ".");
    }

  std::string line;
  std::string content;

  while ( std::getline(file, line) )
    {
      content += line + "\n";
    }
  
  return content;
}

FileReader::~FileReader()
{
  
}
