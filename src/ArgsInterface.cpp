#include <fstream>
#include <ArgsInterface.hpp>
#include "ArgsParser.hpp"
#include "ProjectFactory.hpp"
#include "YamlReader.hpp"
#include "MakefileBuilder.hpp"
#include "MakefileChecker.hpp"
#include "FileCreator.hpp"
#include "FileChecker.hpp"
#include "FileReader.hpp"

ArgsInterface::ArgsInterface(ArgsParser& args_parser,
			     ProjectFactory& project_factory,
			     FileCreator& file_creator,
			     FileChecker& file_checker,
			     FileReader& file_reader)
  : m_args_parser { args_parser }
  , m_project_factory { project_factory }
  , m_file_creator { file_creator }
  , m_file_checker { file_checker }
  , m_file_reader { file_reader }
{
  
}

void ArgsInterface::execute()
{
  std::string action = m_args_parser.getAction();
  std::vector<std::string> params = m_args_parser.getParams();

  if ( action.empty() )
    {
      std::cerr<< "Usage:" <<std::endl;
      std::cerr<< "\t init <project_name>" <<std::endl;
      std::cerr<< "\t update <name>" <<std::endl;
    }
  else if (action == "init")
    {
      if (params.size() == 1)
	{
	  m_project_factory.initSimple( params.back() );	  

	  std::string content = m_file_reader.from("duck.yml");

	  YamlReader yaml_reader { content };

	  MakefileBuilder makefile;
	  makefile.yamlReader(yaml_reader, m_file_checker);	 

	  MakefileChecker checker{m_file_checker, makefile};
	  
	  std::string makefile_source = makefile.build();

	  if ( checker.validate() )
	    {
	      m_file_creator.createFile("Makefile", makefile_source);
	    }
	  else
	    {
	      for (std::string const& err : checker.errors())
		{
		  std::cerr<< err <<std::endl;
		}
	    }
	}
      else
	{
	  throw std::runtime_error("Missing project name");
	}
    }
  else if (action == "update")
    {
      if ( !m_file_checker.exists("duck.yml") )
	{
	  throw std::runtime_error("duck.yml not found");
	}

      std::string content = m_file_reader.from("duck.yml");

      YamlReader yaml_reader { content };

      MakefileBuilder makefile;
      makefile.yamlReader(yaml_reader, m_file_checker);	 

      MakefileChecker checker{m_file_checker, makefile};
      std::string makefile_source = makefile.build();

      if ( checker.validate() )
	{
	  m_file_creator.remove("Makefile");
	  m_file_creator.createFile("Makefile", makefile_source);
	}
      else	
	{
	  for (std::string const& err : checker.errors())
	    {
	      std::cerr<< err <<std::endl;
	    }
	}
    }
  else
    {
      throw std::runtime_error("Flag " + action + " doesn't exist");
    }
}


ArgsInterface::~ArgsInterface()
{
  
}
