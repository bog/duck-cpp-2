#include <filesystem>
#include <algorithm>
#include <MakefileChecker.hpp>
#include <MakefileBuilder.hpp>
#include <FileChecker.hpp>
#include <Sections.hpp>

MakefileChecker::MakefileChecker(FileChecker& file_checker, MakefileBuilder& builder)
  : m_file_checker { file_checker }
  , m_builder { builder }
{
  
}

bool MakefileChecker::validate()
{
  /**********/
  /* ERRORS */
  /**********/
  
  //////////////
  // INSTALL //
  //////////////
  
  Install install = m_builder.getInstall();
  
  if ( !m_file_checker.exists(install.bin_dir) )
    {
      m_errors.push_back("E: [install::bin-dir] cannot find " + install.bin_dir + ".");
      return false;
    }

  if ( !m_file_checker.exists(std::filesystem::path(install.asset_dir).parent_path())
       || m_file_checker.exists(install.asset_dir) )
    {
      m_errors.push_back("E: [install::asset-dir] cannot use " + install.asset_dir + ".");
      return false;
    }
  
  /////////////
  // ASSETS //
  /////////////
  
  std::vector<Asset> assets = m_builder.getAssets();
  
  for (Asset const& asset : assets)
    {
      for (std::string const& dir : asset.dirs)
	{
	  if ( !m_file_checker.exists(dir) )
	    {
	      m_errors.push_back("E: [Assets::asset-dir] cannot find " + dir + ".");
	      return false;
	    }
	}
    }

  
  /////////////
  // COMMON //
  /////////////
  
  Common common = m_builder.getCommon();

  for (std::string const& source : common.sources)
    {
      if ( !m_file_checker.exists(source) )
	{
	  m_errors.push_back("E: [Common::sources] cannot find " + source + ".");
	  return false;
	}
    }

  for (std::string const& include : common.includes)
    {
      if ( !m_file_checker.exists(include) )
	{
	  m_errors.push_back("E: [Common::includes] cannot find " + include + ".");
	  return false;
	}
    }

  if ( !m_file_checker.exists(common.obj_dir) )
    {
      m_errors.push_back("E: [Common::obj-dir] cannot find " + common.obj_dir + ".");
      return false;
    }

  /////////////
  // TARGETS //
  /////////////
  
  std::map<std::string, Target> targets = m_builder.getTargets();

  for (auto const& p : targets)
    {
      Target target = p.second;
      
      for (std::string const& source : target.sources)
	{
	  if ( !m_file_checker.exists(source) )
	    {
	      m_errors.push_back("E: [Target::sources] cannot find " + source + ".");
	      return false;
	    }
	}

      for (std::string const& include : target.includes)
	{
	  if ( !m_file_checker.exists(include) )
	    {
	      m_errors.push_back("E: [Target::includes] cannot find " + include + ".");
	      return false;
	    }
	}

      if ( !m_file_checker.exists(target.obj_dir) )
	{
	  m_errors.push_back("E: [Target::obj-dir] cannot find " + target.obj_dir + ".");
	  return false;
	}

      if ( !target.exec_path.empty() && m_file_checker.exists(target.exec_path))
	{
	  m_errors.push_back("E: [Target::exec-path] "+ target.exec_path +" already exists.");
	  return false;
	}
    }
  
  /************/
  /* WARNINGS */
  /************/

  { // Obj-dir unique
    std::vector<std::string> obj_dirs;
    
    Common common = m_builder.getCommon();
    obj_dirs.push_back(common.obj_dir);
    
    auto targets = m_builder.getTargets();

    for (auto p : targets)
      {
	obj_dirs.push_back(p.second.obj_dir);
      }
    
    size_t size_before = obj_dirs.size();

    obj_dirs.erase(std::unique(obj_dirs.begin(), obj_dirs.end()), obj_dirs.end());

    if ( size_before != obj_dirs.size() )
      {
        m_errors.push_back("W: obj-dir should be unique.");
      }
  }

  { // exec-path unique
    std::vector<std::string> names;
    
    auto targets = m_builder.getTargets();

    for (auto p : targets)
      {
        names.push_back(p.second.exec_path);
      }
    
    size_t size_before = names.size();

    names.erase(std::unique(names.begin(), names.end()), names.end());

    if ( size_before != names.size() )
      {
        m_errors.push_back("W: exec-path should be unique.");
      }
  }
  
  return true;
}

std::vector<std::string> MakefileChecker::errors() const
{
  return m_errors;
}

MakefileChecker::~MakefileChecker()
{
  
}
