#include <sstream>
#include <catch.hpp>
#include "../src/MakefileBuilder.hpp"

TEST_CASE("default", "[MakefileBuilder]")
{
  MakefileBuilder makefile;
	
  std::stringstream output;
  output << "CXXFLAGS= -I ./" << std::endl;
  output << "LDFLAGS=" << std::endl;
  output << "SRC= $(wildcard *.cpp)" << std::endl;
  output << "OBJDIR= ." << std::endl;
  output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;
  output << "gnu64: a.out" << std::endl;
  output << "CXXFLAGS_gnu64=" << std::endl;
  output << "LDFLAGS_gnu64=" << std::endl;
  output << "SRC_gnu64=" << std::endl;
  output << "OBJDIR_gnu64= ." << std::endl;
  output << "OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "run-gnu64: a.out" << std::endl;
  output << "\t" << "./$^" << std::endl;

  output << "a.out:$(OBJ) $(OBJ_gnu64)" << std::endl;
  output << "\t" << "$(CXX) $(CXXFLAGS) $(CXXFLAGS_gnu64) $(OBJ) $(OBJ_gnu64) $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;
  
  output << "install-gnu64: a.out" << std::endl;
  output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
  output << "uninstall-gnu64:" << std::endl;
  output << "\t" << "rm -vir /usr/local/bin/a.out" << std::endl;

  output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;
  
  output << "$(OBJDIR_gnu64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

  output << "check:" << std::endl;
  output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_gnu64)" << std::endl;
  
  output << "clean:" << std::endl;
  output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o" << std::endl;
  output << "mrproper: clean" << std::endl;
  output << "\t" << "rm -f a.out" << std::endl;

  output << ".PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 check clean mrproper" << std::endl;
  
  REQUIRE( output.str() == makefile.build() );
}
