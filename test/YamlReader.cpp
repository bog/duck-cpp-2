#include <map>
#include <sstream>
#include <catch.hpp>
#include "../src/YamlReader.hpp"
#include "../src/Sections.hpp"

TEST_CASE("read-empty", "[YamlReader]")
{
  YamlReader yaml { "" };
  REQUIRE_NOTHROW(yaml.readProject());
  REQUIRE_NOTHROW(yaml.readInstall());
  REQUIRE_NOTHROW(yaml.readAssets());
  REQUIRE_NOTHROW(yaml.readCommon());
  REQUIRE_NOTHROW(yaml.readTargets());  
}

TEST_CASE("read-empty-defaults", "[YamlReader]")
{
  YamlReader yaml { "" };
  
  Project project = yaml.readProject();
  REQUIRE("" == project.name);
  REQUIRE( project.authors.empty() );
  REQUIRE("" == project.license);
  REQUIRE("" == project.version);
  
  Install install = yaml.readInstall();
  REQUIRE("" == install.bin_dir);
  REQUIRE("" == install.asset_dir);
  
  std::vector<Asset> assets = yaml.readAssets();
  REQUIRE( 0 == assets.size() );
  
  Common common = yaml.readCommon();
  REQUIRE( 0 == common.sources.size() );
  REQUIRE( 0 == common.includes.size() );
  REQUIRE( 0 == common.flags.size() );
  REQUIRE( 0 == common.libs.size() );
  REQUIRE("" == common.obj_dir);
  
  std::map<std::string, Target> targets = yaml.readTargets();
  REQUIRE( 0 == targets.size() );
}
TEST_CASE("read-project", "[YamlReader]")
{
  std::stringstream input;
  input << "" << "project:" << std::endl;
  input << " " << "name: my-prog" << std::endl;
  input << " " << "authors:" << std::endl;
  input << "  " << "- bog" << std::endl;
  input << "  " << "- arthur" << std::endl;
  input << " " << "license: GNU GPLv3" << std::endl;
  input << " " << "version: 42" << std::endl;
  
  YamlReader yaml { input.str() };

  Project project = yaml.readProject();
  
  REQUIRE("my-prog" == project.name);
  REQUIRE( 2 == project.authors.size() );
  REQUIRE("bog" == project.authors[0]);
  REQUIRE("arthur" == project.authors[1]);
  REQUIRE("GNU GPLv3" == project.license);
  REQUIRE("42" == project.version);
}

TEST_CASE("read-install", "[YamlReader]")
{
  std::stringstream input;
  input << "install:" << std::endl;
  input << "  bin-dir: /opt/games/bin" << std::endl;
  input << "  asset-dir: /opt/games/share/yaml" << std::endl;
  
  YamlReader yaml { input.str() };

  Install install = yaml.readInstall();
  
  REQUIRE("/opt/games/bin" == install.bin_dir);
  REQUIRE("/opt/games/share/yaml" == install.asset_dir);
}

TEST_CASE("read-assets", "[YamlReader]")
{
  std::stringstream input;
  input << "assets:" << std::endl;
  input << " - asset:" << std::endl;
  input << "    dirs:" << std::endl;
  input << "      - hello" << std::endl;
  input << "      - world" << std::endl;
  input << " - asset:" << std::endl;
  input << "    dirs:" << std::endl;
  input << "      - /opt/hello" << std::endl;
  input << "      - /opt/world" << std::endl;
  
  YamlReader yaml { input.str() };

  std::vector<Asset> assets = yaml.readAssets();

  REQUIRE(2 == assets.size());
  REQUIRE("hello" == assets[0].dirs[0]);
  REQUIRE("world" == assets[0].dirs[1]);
  REQUIRE("/opt/hello" == assets[1].dirs[0]);
  REQUIRE("/opt/world" == assets[1].dirs[1]);
}

TEST_CASE("read-common", "[YamlReader]")
{
  std::stringstream input;
  input << "common:" << std::endl;
  input << " sources:" << std::endl;
  input << "  - src0" << std::endl;
  input << "  - src1" << std::endl;
  input << " includes:" << std::endl;
  input << "  - inc0" << std::endl;
  input << "  - inc1" << std::endl;
  input << " flags:" << std::endl;
  input << "  - flag0" << std::endl;
  input << "  - flag1" << std::endl;
  input << " libs:" << std::endl;
  input << "  - lib0" << std::endl;
  input << "  - lib1" << std::endl;
  input << " obj-dir: a/random/folder" << std::endl;
  
  YamlReader yaml { input.str() };

  Common common = yaml.readCommon();
  
  REQUIRE("src0" == common.sources[0]);
  REQUIRE("src1" == common.sources[1]);
  REQUIRE( 2 == common.includes.size() );
  REQUIRE("inc0" == common.includes[0]);
  REQUIRE("inc1" == common.includes[1]);
  REQUIRE( 2 == common.libs.size() );
  REQUIRE("lib0" == common.libs[0]);
  REQUIRE("lib1" == common.libs[1]);
  REQUIRE( 2 == common.flags.size() );
  REQUIRE("flag0" == common.flags[0]);
  REQUIRE("flag1" == common.flags[1]);
  REQUIRE("a/random/folder" == common.obj_dir);
}

TEST_CASE("read-targets", "[YamlReader]")
{
  std::stringstream input;
  input << "targets:" << std::endl;
  input << "  - target:" << std::endl;
  input << "      name: gnu64" << std::endl;
  input << "      toolchain-prefix: x86_64-w64-mingw32" << std::endl;
  input << "      exec-path: build/gnu/bin" << std::endl;
  input << "      sources:" << std::endl;
  input << "        - src0" << std::endl;
  input << "        - src1" << std::endl;
  input << "      includes: " << std::endl;
  input << "        - inc0" << std::endl;
  input << "        - inc1" << std::endl;
  input << "      libs: " << std::endl;
  input << "        - lib0" << std::endl;
  input << "        - lib1" << std::endl;
  input << "      flags: " << std::endl;
  input << "        - flag0" << std::endl;
  input << "        - flag1" << std::endl;
  input << "      obj-dir: build/obj" << std::endl;

  
  YamlReader yaml { input.str() };

  std::map<std::string, Target> target = yaml.readTargets();  
  
  REQUIRE("gnu64" == target["gnu64"].name);
  REQUIRE("x86_64-w64-mingw32" == target["gnu64"].toolchain_prefix);
  REQUIRE("build/gnu/bin" == target["gnu64"].exec_path);
  REQUIRE( 2 == target["gnu64"].sources.size() );
  REQUIRE("src0" == target["gnu64"].sources[0]);
  REQUIRE("src1" == target["gnu64"].sources[1]);
  REQUIRE( 2 == target["gnu64"].includes.size() );
  REQUIRE("inc0" == target["gnu64"].includes[0]);
  REQUIRE("inc1" == target["gnu64"].includes[1]);
  REQUIRE( 2 == target["gnu64"].libs.size() );
  REQUIRE("lib0" == target["gnu64"].libs[0]);
  REQUIRE("lib1" == target["gnu64"].libs[1]);
  REQUIRE( 2 == target["gnu64"].flags.size() );
  REQUIRE("flag0" == target["gnu64"].flags[0]);
  REQUIRE("flag1" == target["gnu64"].flags[1]);
  REQUIRE("build/obj" == target["gnu64"].obj_dir);
}
