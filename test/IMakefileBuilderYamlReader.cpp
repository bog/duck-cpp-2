#include <sstream>
#include <catch.hpp>
#include "../src/MakefileBuilder.hpp"
#include "../src/YamlReader.hpp"
#include "../src/Sections.hpp"
#include "FileCheckerMock.hpp"
TEST_CASE("imake-reader-project", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "project:" << std::endl;
  input << "  name: my-project" << std::endl;
  input << "  authors:" << std::endl;
  input << "    - bog" << std::endl;
  input << "    - vlad" << std::endl;
  input << "  license: CC-BY-SA" << std::endl;
  input << "  version: 4" << std::endl;

  YamlReader yaml { input.str() };
  
  MakefileBuilder makefile;
  FileCheckerMock file;
  makefile.yamlReader(yaml, file);

  Project project = makefile.getProject();
  REQUIRE("my-project" == project.name);
  REQUIRE( 2 == project.authors.size() );
  REQUIRE("bog" == project.authors[0]);
  REQUIRE("vlad" == project.authors[1]);
  REQUIRE("CC-BY-SA" == project.license);
  REQUIRE("4" == project.version);
}


TEST_CASE("imake-reader-install", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "install:" << std::endl;
  input << "  bin-dir: /bin/dir" << std::endl;
  input << "  asset-dir: /asset/dir" << std::endl;

  YamlReader yaml { input.str() };
  
  MakefileBuilder makefile;

  FileCheckerMock file;
  makefile.yamlReader(yaml, file);

  Install install = makefile.getInstall();
  REQUIRE("/bin/dir" == install.bin_dir);
  REQUIRE("/asset/dir" == install.asset_dir);
}

TEST_CASE("imake-reader-assets", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "assets:" << std::endl;
  input << "  - asset:" << std::endl;
  input << "      dirs:" << std::endl;
  input << "        - assets/dir0" << std::endl;
  input << "        - assets/dir1" << std::endl;

  YamlReader yaml { input.str() };
  
  MakefileBuilder makefile;
  
  FileCheckerMock file;
  makefile.yamlReader(yaml, file);

  std::vector<Asset> assets = makefile.getAssets();
  
  REQUIRE( 1 == assets.size() );
  REQUIRE( 2 == assets[0].dirs.size() );
  REQUIRE("assets/dir0" == assets[0].dirs[0]);
  REQUIRE("assets/dir1" == assets[0].dirs[1]);
}

TEST_CASE("imake-reader-common", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "common:" << std::endl;
  input << "  sources:" << std::endl;
  input << "    - common/src0" << std::endl;
  input << "    - common/src1" << std::endl;
  input << "  includes:" << std::endl;
  input << "    - common/inc0" << std::endl;
  input << "    - common/inc1" << std::endl;
  input << "  flags:" << std::endl;
  input << "    - common/flag0" << std::endl;
  input << "    - common/flag1" << std::endl;
  input << "  libs:" << std::endl;
  input << "    - common/lib0" << std::endl;
  input << "    - common/lib1" << std::endl;
  input << "  obj-dir: build/obj" << std::endl;

  YamlReader yaml { input.str() };
  
  MakefileBuilder makefile;
  FileCheckerMock file;
  file.dirs.push_back("common/lib0");
  file.dirs.push_back("common/lib1");
  makefile.yamlReader(yaml, file);

  Common common = makefile.getCommon();
  
  REQUIRE( 2 == common.sources.size() );
  REQUIRE( "common/src0" == common.sources[0] );
  REQUIRE( "common/src1" == common.sources[1] );

  REQUIRE( 2 == common.includes.size() );
  REQUIRE( "common/inc0" == common.includes[0] );
  REQUIRE( "common/inc1" == common.includes[1] );

  REQUIRE( 2 == common.flags.size() );
  REQUIRE( "common/flag0" == common.flags[0] );
  REQUIRE( "common/flag1" == common.flags[1] );

  REQUIRE( 2 == common.libs.size() );
  REQUIRE( "-L common/lib0" == common.libs[0] );
  REQUIRE( "-L common/lib1" == common.libs[1] );

  REQUIRE("build/obj" == common.obj_dir);
}

TEST_CASE("imake-reader-target", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "targets:" << std::endl;
  input << "  - target:" << std::endl;
  input << "      name: gnu64" << std::endl;
  input << "      toolchain-prefix: azerty" << std::endl;
  input << "      exec-path: gnu/bin" << std::endl;
  input << "      sources:" << std::endl;
  input << "        - target0/src0" << std::endl;
  input << "        - target0/src1" << std::endl;
  input << "      includes:" << std::endl;
  input << "        - target0/inc0" << std::endl;
  input << "        - target0/inc1" << std::endl;
  input << "      flags:" << std::endl;
  input << "        - target0/flag0" << std::endl;
  input << "        - target0/flag1" << std::endl;
  input << "      libs:" << std::endl;
  input << "        - target0/lib0" << std::endl;
  input << "        - target0/lib1" << std::endl;
  input << "      obj-dir: target0/obj" << std::endl;
 
  YamlReader yaml { input.str() };
  
  MakefileBuilder makefile;
  FileCheckerMock file;
  file.dirs.push_back("target0/lib0");
  file.dirs.push_back("target0/lib1");
  
  makefile.yamlReader(yaml, file);

  std::map<std::string, Target> targets = makefile.getTargets();

  REQUIRE( 1 == targets.size() );
  REQUIRE( "gnu64" == targets["gnu64"].name );
  REQUIRE( "azerty" == targets["gnu64"].toolchain_prefix );
  REQUIRE( "gnu/bin" == targets["gnu64"].exec_path );
  
  REQUIRE( "target0/src0" == targets["gnu64"].sources[0] );
  REQUIRE( "target0/src1" == targets["gnu64"].sources[1] );

  REQUIRE( 2 == targets["gnu64"].includes.size() );
  REQUIRE( "target0/inc0" == targets["gnu64"].includes[0] );
  REQUIRE( "target0/inc1" == targets["gnu64"].includes[1] );

  REQUIRE( 2 == targets["gnu64"].flags.size() );
  REQUIRE( "target0/flag0" == targets["gnu64"].flags[0] );
  REQUIRE( "target0/flag1" == targets["gnu64"].flags[1] );

  REQUIRE( 2 == targets["gnu64"].libs.size() );
  REQUIRE( "-L target0/lib0" == targets["gnu64"].libs[0] );
  REQUIRE( "-L target0/lib1" == targets["gnu64"].libs[1] );

  REQUIRE("target0/obj" == targets["gnu64"].obj_dir);
}


TEST_CASE("imake-reader-target-no-toolchain", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "targets:" << std::endl;
  input << "  - target:" << std::endl;
  input << "      name: gnu64" << std::endl;
  input << "      toolchain-prefix:" << std::endl;
  input << "      exec-path: gnu/bin" << std::endl;
  input << "      sources:" << std::endl;
  input << "        - target0/src0" << std::endl;
  input << "        - target0/src1" << std::endl;
  input << "      includes:" << std::endl;
  input << "        - target0/inc0" << std::endl;
  input << "        - target0/inc1" << std::endl;
  input << "      flags:" << std::endl;
  input << "        - target0/flag0" << std::endl;
  input << "        - target0/flag1" << std::endl;
  input << "      libs:" << std::endl;
  input << "        - target0/lib0" << std::endl;
  input << "        - target0/lib1" << std::endl;
  input << "      obj-dir: target0/obj" << std::endl;
 
  YamlReader yaml { input.str() };
  
  MakefileBuilder makefile;
  FileCheckerMock file;
  file.dirs.push_back("target0/lib0");
  file.dirs.push_back("target0/lib1");
  makefile.yamlReader(yaml, file);

  std::map<std::string, Target> targets = makefile.getTargets();

  REQUIRE( 1 == targets.size() );
  REQUIRE( "gnu64" == targets["gnu64"].name );
  REQUIRE( "gnu/bin" == targets["gnu64"].exec_path );
  
  REQUIRE( "target0/src0" == targets["gnu64"].sources[0] );
  REQUIRE( "target0/src1" == targets["gnu64"].sources[1] );

  REQUIRE( 2 == targets["gnu64"].includes.size() );
  REQUIRE( "target0/inc0" == targets["gnu64"].includes[0] );
  REQUIRE( "target0/inc1" == targets["gnu64"].includes[1] );

  REQUIRE( 2 == targets["gnu64"].flags.size() );
  REQUIRE( "target0/flag0" == targets["gnu64"].flags[0] );
  REQUIRE( "target0/flag1" == targets["gnu64"].flags[1] );

  REQUIRE( 2 == targets["gnu64"].libs.size() );
  REQUIRE( "-L target0/lib0" == targets["gnu64"].libs[0] );
  REQUIRE( "-L target0/lib1" == targets["gnu64"].libs[1] );

  REQUIRE("target0/obj" == targets["gnu64"].obj_dir);
}
