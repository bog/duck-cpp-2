#include <catch.hpp>
#include "../src/ProjectFactory.hpp"
#include "../src/YamlFactory.hpp"
#include "FileCreatorMock.hpp"
#include "FileCheckerMock.hpp"

TEST_CASE("ipfy-init-simple", "[IArgsProjectFile]")
{
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  
  YamlFactory yaml_factory;

  ProjectFactory project(file_creator, file_checker, yaml_factory);  
  project.initSimple("name");

  REQUIRE( 2 == file_creator.filesCreated().size() );
  REQUIRE( "src/name.cpp" == file_creator.filesCreated()[0].first );
  REQUIRE( "duck.yml" == file_creator.filesCreated()[1].first );  
}

TEST_CASE("ipfy-init-simple-error-0", "[IArgsProjectFile]")
{
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  file_checker.ok.push_back("src");
  
  YamlFactory yaml_factory;

  ProjectFactory project(file_creator, file_checker, yaml_factory);  

  REQUIRE_THROWS( project.initSimple("name") );
}

TEST_CASE("ipfy-init-simple-error-1", "[IArgsProjectFile]")
{
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  file_checker.ok.push_back("duck.yml");
  
  YamlFactory yaml_factory;

  ProjectFactory project(file_creator, file_checker, yaml_factory);  

  REQUIRE_THROWS( project.initSimple("name") );
}
