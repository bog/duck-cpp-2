#include <sstream>
#include <catch.hpp>
#include "../src/MakefileBuilder.hpp"
#include "FileCheckerMock.hpp"

TEST_CASE("common", "[MakefileBuilderCommon]")
{
  SECTION("common-default")
    {
      FileCheckerMock checker;

      MakefileBuilder makefile;
      makefile.commonSources({})
	.commonObjDir("")
	.commonIncludes({})
	.commonFlags({})
	.commonLibs({}, checker);
      
      Common common = makefile.getCommon();
      
      REQUIRE( 0 == common.sources.size() );
      REQUIRE( 0 == common.includes.size() );
      REQUIRE( 0 == common.flags.size() );
      REQUIRE( 0 == common.libs.size() );
      REQUIRE( "." == common.obj_dir );
    }

  SECTION("Fully-specified-with-paths")
    {
      FileCheckerMock checker;
      
      MakefileBuilder makefile;
      makefile.commonSources({"Hello.cpp", "World.cpp"})
	.commonObjDir("build/obj")
	.commonIncludes({"Hello.hpp", "World.hpp"})
	.commonFlags({"-Wall", "-g"})
	.commonLibs({"sdl2", "yaml-cpp"}, checker);
      
      std::stringstream output;
      output << "CXXFLAGS= -I Hello.hpp -I World.hpp -Wall -g" << std::endl;
      output << "LDFLAGS= $(shell pkg-config --libs --cflags sdl2) $(shell pkg-config --libs --cflags yaml-cpp)" << std::endl;
      output << "SRC= Hello.cpp World.cpp" << std::endl;
      output << "OBJDIR= build/obj" << std::endl;
      output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;
      output << "gnu64: a.out" << std::endl;
      output << "CXXFLAGS_gnu64=" << std::endl;
      output << "LDFLAGS_gnu64=" << std::endl;
      output << "SRC_gnu64=" << std::endl;
      output << "OBJDIR_gnu64= ." << std::endl;
      output << "OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

      output << "run-gnu64: a.out" << std::endl;
      output << "\t" << "./$^" << std::endl;

      output << "a.out:$(OBJ) $(OBJ_gnu64)" << std::endl;
      output << "\t" << "$(CXX) $(CXXFLAGS) $(CXXFLAGS_gnu64) $(OBJ) $(OBJ_gnu64) $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

      output << "install-gnu64: a.out" << std::endl;
      output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
      output << "uninstall-gnu64:" << std::endl;
      output << "\t" << "rm -vir /usr/local/bin/a.out" << std::endl;

      output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;
  
      output << "$(OBJDIR_gnu64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;
      
      output << "check:" << std::endl;
      output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_gnu64)" << std::endl;

      output << "clean:" << std::endl;
      output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o" << std::endl;
      output << "mrproper: clean" << std::endl;
      output << "\t" << "rm -f a.out" << std::endl;

      output << ".PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 check clean mrproper" << std::endl;
      
      REQUIRE( output.str() == makefile.build() );
    }

  SECTION("bug-libs")
    {
      FileCheckerMock checker;
      
      MakefileBuilder makefile;
      makefile
	.commonObjDir("build/obj")
	.commonSources({"Hello.cpp", "src/World"})
	.commonIncludes({"Hello.hpp", "inc/World"})
	.commonFlags({"-Wall", "-g"})
	.commonLibs({"sdl2", "-lsfml-graphics"}, checker);
      
      std::stringstream output;
      output << "CXXFLAGS= -I inc/World -I Hello.hpp -Wall -g" << std::endl;
      output << "LDFLAGS= $(shell pkg-config --libs --cflags sdl2) -lsfml-graphics" << std::endl;
      output << "SRC= $(wildcard src/World/*.cpp) Hello.cpp" << std::endl;
      output << "OBJDIR= build/obj" << std::endl;
      output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;
      output << "gnu64: a.out" << std::endl;
      output << "CXXFLAGS_gnu64=" << std::endl;
      output << "LDFLAGS_gnu64=" << std::endl;
      output << "SRC_gnu64=" << std::endl;
      output << "OBJDIR_gnu64= ." << std::endl;
      output << "OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

      output << "run-gnu64: a.out" << std::endl;
      output << "\t" << "./$^" << std::endl;

      output << "a.out:$(OBJ) $(OBJ_gnu64)" << std::endl;
      output << "\t" << "$(CXX) $(CXXFLAGS) $(CXXFLAGS_gnu64) $(OBJ) $(OBJ_gnu64) $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

      output << "install-gnu64: a.out" << std::endl;
      output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
      output << "uninstall-gnu64:" << std::endl;
      output << "\t" << "rm -vir /usr/local/bin/a.out" << std::endl;

      output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

      output << "$(OBJDIR)/%.o:src/World/%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

      output << "$(OBJDIR_gnu64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;
      
      output << "check:" << std::endl;
      output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_gnu64)" << std::endl;

      output << "clean:" << std::endl;
      output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o" << std::endl;
      output << "mrproper: clean" << std::endl;
      output << "\t" << "rm -f a.out" << std::endl;

      output << ".PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 check clean mrproper" << std::endl;
	
      REQUIRE( output.str() == makefile.build() );
    }
    
  SECTION("Fully-specified-with-paths-and-dirs")
    {
      FileCheckerMock checker;
      
      MakefileBuilder makefile;
      makefile
	.commonObjDir("build/obj")
	.commonSources({"Hello.cpp", "src/World"})
	.commonIncludes({"Hello.hpp", "inc/World"})
	.commonFlags({"-Wall", "-g"})
	.commonLibs({"sdl2", "yaml-cpp"}, checker);
      
      std::stringstream output;
      output << "CXXFLAGS= -I inc/World -I Hello.hpp -Wall -g" << std::endl;
      output << "LDFLAGS= $(shell pkg-config --libs --cflags sdl2) $(shell pkg-config --libs --cflags yaml-cpp)" << std::endl;
      output << "SRC= $(wildcard src/World/*.cpp) Hello.cpp" << std::endl;
      output << "OBJDIR= build/obj" << std::endl;
      output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;
      output << "gnu64: a.out" << std::endl;
      output << "CXXFLAGS_gnu64=" << std::endl;
      output << "LDFLAGS_gnu64=" << std::endl;
      output << "SRC_gnu64=" << std::endl;
      output << "OBJDIR_gnu64= ." << std::endl;
      output << "OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

      output << "run-gnu64: a.out" << std::endl;
      output << "\t" << "./$^" << std::endl;

      output << "a.out:$(OBJ) $(OBJ_gnu64)" << std::endl;
      output << "\t" << "$(CXX) $(CXXFLAGS) $(CXXFLAGS_gnu64) $(OBJ) $(OBJ_gnu64) $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

      output << "install-gnu64: a.out" << std::endl;
      output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
      output << "uninstall-gnu64:" << std::endl;
      output << "\t" << "rm -vir /usr/local/bin/a.out" << std::endl;

      output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

      output << "$(OBJDIR)/%.o:src/World/%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

      output << "$(OBJDIR_gnu64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;
      
      output << "check:" << std::endl;
      output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_gnu64)" << std::endl;

      output << "clean:" << std::endl;
      output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o" << std::endl;
      output << "mrproper: clean" << std::endl;
      output << "\t" << "rm -f a.out" << std::endl;

      output << ".PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 check clean mrproper" << std::endl;
	
      REQUIRE( output.str() == makefile.build() );
    }

  SECTION("Fully-specified-with-paths-and-dirs-order-independant")
    {
      FileCheckerMock checker;
      MakefileBuilder makefile;
      makefile
	.commonObjDir("build/obj")
	.commonLibs({"sdl2", "yaml-cpp"}, checker)
	.commonIncludes({"Hello.hpp", "inc/World"})
	.commonFlags({"-Wall", "-g"})
	.commonSources({"Hello.cpp", "src/World"});
      
      std::stringstream output;
      output << "CXXFLAGS= -I inc/World -I Hello.hpp -Wall -g" << std::endl;
      output << "LDFLAGS= $(shell pkg-config --libs --cflags sdl2) $(shell pkg-config --libs --cflags yaml-cpp)" << std::endl;
      output << "SRC= $(wildcard src/World/*.cpp) Hello.cpp" << std::endl;
      output << "OBJDIR= build/obj" << std::endl;
      output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;
      output << "gnu64: a.out" << std::endl;
      output << "CXXFLAGS_gnu64=" << std::endl;
      output << "LDFLAGS_gnu64=" << std::endl;
      output << "SRC_gnu64=" << std::endl;
      output << "OBJDIR_gnu64= ." << std::endl;
      output << "OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

      output << "run-gnu64: a.out" << std::endl;
      output << "\t" << "./$^" << std::endl;

      output << "a.out:$(OBJ) $(OBJ_gnu64)" << std::endl;
      output << "\t" << "$(CXX) $(CXXFLAGS) $(CXXFLAGS_gnu64) $(OBJ) $(OBJ_gnu64) $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

      output << "install-gnu64: a.out" << std::endl;
      output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
      output << "uninstall-gnu64:" << std::endl;
      output << "\t" << "rm -vir /usr/local/bin/a.out" << std::endl;

      output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

      output << "$(OBJDIR)/%.o:src/World/%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

      output << "$(OBJDIR_gnu64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

      output << "check:" << std::endl;
      output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_gnu64)" << std::endl;

      output << "clean:" << std::endl;
      output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o" << std::endl;
      output << "mrproper: clean" << std::endl;
      output << "\t" << "rm -f a.out" << std::endl;

      output << ".PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 check clean mrproper" << std::endl;
      
      REQUIRE( output.str() == makefile.build() );
    }

  SECTION("Fully-specified-with-slashes")
    {
      FileCheckerMock checker;
      
      MakefileBuilder makefile;
      makefile
	.commonObjDir("build/obj")
	.commonSources({"Hello.cpp", "src/World//"})
	.commonIncludes({"Hello.hpp", "inc/World/"})
	.commonFlags({"-Wall", "-g"})
	.commonLibs({"sdl2", "yaml-cpp"}, checker);
      
      std::stringstream output;
      output << "CXXFLAGS= -I inc/World -I Hello.hpp -Wall -g" << std::endl;
      output << "LDFLAGS= $(shell pkg-config --libs --cflags sdl2) $(shell pkg-config --libs --cflags yaml-cpp)" << std::endl;
      output << "SRC= $(wildcard src/World/*.cpp) Hello.cpp" << std::endl;
      output << "OBJDIR= build/obj" << std::endl;
      output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;      
      output << "gnu64: a.out" << std::endl;
      output << "CXXFLAGS_gnu64=" << std::endl;
      output << "LDFLAGS_gnu64=" << std::endl;
      output << "SRC_gnu64=" << std::endl;
      output << "OBJDIR_gnu64= ." << std::endl;
      output << "OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

      output << "run-gnu64: a.out" << std::endl;
      output << "\t" << "./$^" << std::endl;

      output << "a.out:$(OBJ) $(OBJ_gnu64)" << std::endl;
      output << "\t" << "$(CXX) $(CXXFLAGS) $(CXXFLAGS_gnu64) $(OBJ) $(OBJ_gnu64) $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

      output << "install-gnu64: a.out" << std::endl;
      output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
      output << "uninstall-gnu64:" << std::endl;
      output << "\t" << "rm -vir /usr/local/bin/a.out" << std::endl;

      output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

      output << "$(OBJDIR)/%.o:src/World/%.cpp" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

      output << "$(OBJDIR_gnu64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
      output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

      output << "check:" << std::endl;
      output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_gnu64)" << std::endl;

      output << "clean:" << std::endl;
      output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o" << std::endl;
      output << "mrproper: clean" << std::endl;
      output << "\t" << "rm -f a.out" << std::endl;

      output << ".PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 check clean mrproper" << std::endl;
	
      REQUIRE( output.str() == makefile.build() );
    }

    SECTION("Fully-four-libs")
      {
	FileCheckerMock checker;
	checker.dirs.push_back("libdir");
	checker.files.push_back("libfile");
	
	MakefileBuilder makefile;
	makefile
	  .commonObjDir("build/obj")
	  .commonSources({"Hello.cpp", "src/World"})
	  .commonIncludes({"Hello.hpp", "inc/World"})
	  .commonFlags({"-Wall", "-g"})
	  .commonLibs({"libpkg", "libfile", "-libtag", "libdir"}, checker);
      
	std::stringstream output;
	output << "CXXFLAGS= -I inc/World -I Hello.hpp -Wall -g" << std::endl;
	output << "LDFLAGS= $(shell pkg-config --libs --cflags libpkg) -L libdir -l libfile -libtag" << std::endl;
	output << "SRC= $(wildcard src/World/*.cpp) Hello.cpp" << std::endl;
	output << "OBJDIR= build/obj" << std::endl;
	output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;
	output << "gnu64: a.out" << std::endl;
	output << "CXXFLAGS_gnu64=" << std::endl;
	output << "LDFLAGS_gnu64=" << std::endl;
	output << "SRC_gnu64=" << std::endl;
	output << "OBJDIR_gnu64= ." << std::endl;
	output << "OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

	output << "run-gnu64: a.out" << std::endl;
	output << "\t" << "./$^" << std::endl;

	output << "a.out:$(OBJ) $(OBJ_gnu64)" << std::endl;
	output << "\t" << "$(CXX) $(CXXFLAGS) $(CXXFLAGS_gnu64) $(OBJ) $(OBJ_gnu64) $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

	output << "install-gnu64: a.out" << std::endl;
	output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
	output << "uninstall-gnu64:" << std::endl;
	output << "\t" << "rm -vir /usr/local/bin/a.out" << std::endl;

	output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
	output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

	output << "$(OBJDIR)/%.o:src/World/%.cpp" << std::endl;
	output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

	output << "$(OBJDIR_gnu64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
	output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;
      
	output << "check:" << std::endl;
	output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_gnu64)" << std::endl;

	output << "clean:" << std::endl;
	output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o" << std::endl;
	output << "mrproper: clean" << std::endl;
	output << "\t" << "rm -f a.out" << std::endl;

	output << ".PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 check clean mrproper" << std::endl;
	
	REQUIRE( output.str() == makefile.build() );
      }
}
