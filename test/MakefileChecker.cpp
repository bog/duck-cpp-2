#include <sstream>
#include <catch.hpp>
#include "../src/MakefileBuilder.hpp"
#include "../src/MakefileChecker.hpp"
#include "../src/FileChecker.hpp"
#include "FileCheckerMock.hpp"

TEST_CASE("checker-install-ok", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .installBinDir("/usr/local/bin")
    .installAssetDir("/usr/local/share/hello");
  
  FileCheckerMock file;
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back(".");
  file.ok.push_back("");

  MakefileChecker checker { file, builder };
  
  REQUIRE(true == checker.validate());
}

TEST_CASE("checker-install-ko", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .installBinDir("/usr/local/bin")
    .installAssetDir("/usr/local/share/hello");
  
  FileCheckerMock file;
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share/hello");
  
  MakefileChecker checker { file, builder };
  
  REQUIRE(false == checker.validate());
}

TEST_CASE("checker-assets-ok", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .assetDirs({"a", "b", "c"})
    .assetDirs({"d", "e", "f"});
  
  FileCheckerMock file;
  file.ok.push_back("a");
  file.ok.push_back("b");
  file.ok.push_back("c");
  file.ok.push_back("d");
  file.ok.push_back("e");
  file.ok.push_back("f");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back(".");
  file.ok.push_back("");
  
  MakefileChecker checker { file, builder };
  
  REQUIRE(true == checker.validate());
}

TEST_CASE("checker-assets-ko", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .assetDirs({"a", "b", "c"})
    .assetDirs({"d", "e", "f"});
  
  FileCheckerMock file;
  file.ok.push_back("a");
  file.ok.push_back("b");
  file.ok.push_back("d");
  file.ok.push_back("e");
  file.ok.push_back("f");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back("");

  MakefileChecker checker { file, builder };
  
  REQUIRE(false == checker.validate());
}

TEST_CASE("checker-common-ok", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .commonSources({"src/hello", "src/world", "src/file.cpp"})
    .commonIncludes({"inc/hello", "inc/world", "inc/file.hpp"})
    .commonObjDir("build/bin");
  
  FileCheckerMock file;
  file.ok.push_back("src/hello");
  file.ok.push_back("src/world");
  file.ok.push_back("src/file.cpp");
  file.ok.push_back("inc/hello");
  file.ok.push_back("inc/world");
  file.ok.push_back("inc/file.hpp");
  file.ok.push_back("build/bin");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back("");

  MakefileChecker checker { file, builder };
  
  REQUIRE(true == checker.validate());
}

TEST_CASE("checker-common-ko-src", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .commonSources({"src/hello", "src/world", "src/file.cpp"})
    .commonIncludes({"inc/hello", "inc/world", "inc/file.hpp"})
    .commonObjDir("build/bin");
  
  FileCheckerMock file;
  file.ok.push_back("src/world");
  file.ok.push_back("src/file.cpp");
  file.ok.push_back("inc/hello");
  file.ok.push_back("inc/world");
  file.ok.push_back("inc/file.hpp");
  file.ok.push_back("build/bin");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back("");
  
  MakefileChecker checker { file, builder };
  
  REQUIRE(false == checker.validate());
}

TEST_CASE("checker-common-ko-include", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .commonSources({"src/hello", "src/world", "src/file.cpp"})
    .commonIncludes({"inc/hello", "inc/world", "inc/file.hpp"})
    .commonObjDir("build/bin");
  
  FileCheckerMock file;
  file.ok.push_back("src/hello");
  file.ok.push_back("src/world");
  file.ok.push_back("src/file.cpp");
  file.ok.push_back("inc/hello");
  file.ok.push_back("inc/file.hpp");
  file.ok.push_back("build/bin");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back("");

  MakefileChecker checker { file, builder };
  
  REQUIRE(false == checker.validate());
}

TEST_CASE("checker-common-ko-build", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .commonSources({"src/hello", "src/world", "src/file.cpp"})
    .commonIncludes({"inc/hello", "inc/world", "inc/file.hpp"})
    .commonObjDir("build/bin");
  
  FileCheckerMock file;
  file.ok.push_back("src/hello");
  file.ok.push_back("src/world");
  file.ok.push_back("src/file.cpp");
  file.ok.push_back("inc/hello");
  file.ok.push_back("inc/world");
  file.ok.push_back("inc/file.hpp");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back("");

  MakefileChecker checker { file, builder };
  
  REQUIRE(false == checker.validate());
}

TEST_CASE("checker-target-ok", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .targetExecPath("name", "build/bin")
    .targetSources("name", {"src/s0", "src/s1.cpp"})
    .targetIncludes("name", {"inc/i0.hpp", "inc/i1"})
    .targetObjDir("name", "build/obj");
  
  FileCheckerMock file;
  file.ok.push_back("build");
  file.ok.push_back("src/s0");
  file.ok.push_back("src/s1.cpp");
  file.ok.push_back("inc/i0.hpp");
  file.ok.push_back("inc/i1");
  file.ok.push_back("build/obj");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back(".");
  file.ok.push_back("");
  
  MakefileChecker checker { file, builder };
  
  REQUIRE(true == checker.validate());
}


TEST_CASE("checker-target-ko-exec-path", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .targetExecPath("name", "build/bin")
    .targetSources("name", {"src/s0", "src/s1.cpp"})
    .targetIncludes("name", {"inc/i0.hpp", "inc/i1"})
    .targetObjDir("name", "build/obj");
  
  FileCheckerMock file;
  file.ok.push_back("src/s0");
  file.ok.push_back("src/s1.cpp");
  file.ok.push_back("inc/i0.hpp");
  file.ok.push_back("inc/i1");
  file.ok.push_back("build/obj");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back("");
  
  MakefileChecker checker { file, builder };
  
  REQUIRE(false == checker.validate());
}

TEST_CASE("checker-target-ko-source", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .targetExecPath("name", "build/bin")
    .targetSources("name", {"src/s0", "src/s1.cpp"})
    .targetIncludes("name", {"inc/i0.hpp", "inc/i1"})
    .targetObjDir("name", "build/obj");
  
  FileCheckerMock file;
  file.ok.push_back("build/bin");
  file.ok.push_back("src/s1.cpp");
  file.ok.push_back("inc/i0.hpp");
  file.ok.push_back("inc/i1");
  file.ok.push_back("build/obj");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back("");
  
  MakefileChecker checker { file, builder };
  
  REQUIRE(false == checker.validate());
}

TEST_CASE("checker-target-ko-includes", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .targetExecPath("name", "build/bin")
    .targetSources("name", {"src/s0", "src/s1.cpp"})
    .targetIncludes("name", {"inc/i0.hpp", "inc/i1"})
    .targetObjDir("name", "build/obj");
  
  FileCheckerMock file;
  file.ok.push_back("build/bin");
  file.ok.push_back("src/s0");
  file.ok.push_back("src/s1.cpp");
  file.ok.push_back("inc/i1");
  file.ok.push_back("build/obj");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back("");
  
  MakefileChecker checker { file, builder };
  
  REQUIRE(false == checker.validate());
}

TEST_CASE("checker-target-ko-obj-dir", "[MakefileChecker]")
{
  MakefileBuilder builder;
  builder
    .targetExecPath("name", "build/bin")
    .targetSources("name", {"src/s0", "src/s1.cpp"})
    .targetIncludes("name", {"inc/i0.hpp", "inc/i1"})
    .targetObjDir("name", "build/obj");
  
  FileCheckerMock file;
  file.ok.push_back("build/bin");
  file.ok.push_back("src/s0");
  file.ok.push_back("src/s1.cpp");
  file.ok.push_back("inc/i0");
  file.ok.push_back("inc/i1");
  file.ok.push_back("/usr/local/bin");
  file.ok.push_back("/usr/local/share");
  file.ok.push_back("");
  
  MakefileChecker checker { file, builder };
  
  REQUIRE(false == checker.validate());
}

