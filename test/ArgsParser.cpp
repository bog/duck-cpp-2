#include <catch.hpp>
#include "../src/ArgsParser.hpp"

TEST_CASE("args-empty", "[ArgsParser]")
{
  ArgsParser args({});  
 
  REQUIRE( 0 == args.size() );  
}

TEST_CASE("args-init", "[ArgsParser]")
{
  ArgsParser args({"init"});  
 
  REQUIRE( 1 == args.size() );
  REQUIRE( "init" == args.getAction() );
  REQUIRE( 0 == args.getParams().size() );
}

TEST_CASE("args-create", "[ArgsParser]")
{
  ArgsParser args({"create", "hello"});  
 
  REQUIRE( 2 == args.size() );
  REQUIRE( "create" == args.getAction() );
  REQUIRE( 1 == args.getParams().size() );
  // REQUIRE( "hello" == args.getParams()[0] );
}
