#include <sstream>
#include <catch.hpp>
#include "../src/MakefileBuilder.hpp"
#include "FileCheckerMock.hpp"
TEST_CASE("one-target", "[MakefileBuilderTargets]")
{
  std::stringstream output;
  
  output << "CXXFLAGS= -I ./" << std::endl;
  output << "LDFLAGS=" << std::endl;
  output << "SRC= $(wildcard *.cpp)" << std::endl;
  output << "OBJDIR= ." << std::endl;
  output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;
  output << "win64: build/prog.exe" << std::endl;
  output << "CXXFLAGS_win64= -I includes -I include.hpp -Wall -g -std=c++17" << std::endl;
  output << "LDFLAGS_win64= $(shell pkg-config --libs --cflags sdl2) -lsfml-graphics" << std::endl;
  output << "SRC_win64= $(wildcard sources/*.cpp) source.cpp" << std::endl;
  output << "OBJDIR_win64= build/obj" << std::endl;
  output << "OBJ_win64= $(foreach src, $(SRC_win64), $(OBJDIR_win64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "run-win64: build/prog.exe" << std::endl;
  output << "\t" << "./$^" << std::endl;
  output << "build/prog.exe:$(OBJ) $(OBJ_win64) $(wildcard includes/*.hpp) include.hpp" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) $(CXXFLAGS) $(CXXFLAGS_win64) $(OBJ) $(OBJ_win64) $(LDFLAGS) $(LDFLAGS_win64) -o $@"
	 << std::endl;

  output << "install-win64: build/prog.exe" << std::endl;
  output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
  output << "uninstall-win64:" << std::endl;
  output << "\t" << "rm -vir /usr/local/bin/prog.exe" << std::endl;

  output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

  output << "$(OBJDIR_win64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;

  output << "$(OBJDIR_win64)/%.o:sources/%.cpp $(wildcard sources/*.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;
  
  output << "check:" << std::endl;
  output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_win64)" << std::endl;

  output << "clean:" << std::endl;
  output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_win64)/*.o" << std::endl;
  output << "mrproper: clean" << std::endl;
  output << "\t" << "rm -f build/prog.exe" << std::endl;

  output << ".PHONY: win64 run-win64 install-win64 uninstall-win64 check clean mrproper" << std::endl;
  
  MakefileBuilder makefile;
  FileCheckerMock checker;
  makefile
    .targetObjDir("win64", "build/obj")
    .targetToolchainPrefix("win64", "x86_64-w64-mingw32")
    .targetExecPath("win64", "build/prog.exe")
    .targetSources("win64", {"source.cpp", "sources"})
    .targetIncludes("win64", {"include.hpp", "includes"})
    .targetLibs("win64", {"-lsfml-graphics", "sdl2"}, checker)
    .targetFlags("win64", {"-Wall", "-g", "-std=c++17"});
  
  REQUIRE(output.str() == makefile.build());
}

TEST_CASE("two-targets", "[MakefileBuilderTargets]")
{
  std::stringstream output;
  output << "CXXFLAGS= -I ./" << std::endl;
  output << "LDFLAGS=" << std::endl;
  output << "SRC= $(wildcard *.cpp)" << std::endl;
  output << "OBJDIR= ." << std::endl;
  output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "gnu64: build/prog.elf" << std::endl;
  output << "CXXFLAGS_gnu64= -I gnu_includes -I gnu_include.hpp -Wall -g -std=c++17" << std::endl;
  output << "LDFLAGS_gnu64= $(shell pkg-config --libs --cflags sdl2) -lsfml-graphics" << std::endl;
  output << "SRC_gnu64= $(wildcard gnu_sources/*.cpp) gnu_source.cpp" << std::endl;
  output << "OBJDIR_gnu64= build/obj" << std::endl;
  output << "OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "run-gnu64: build/prog.elf" << std::endl;
  output << "\t" << "./$^" << std::endl;

  output << "build/prog.elf:$(OBJ) $(OBJ_gnu64) $(wildcard gnu_includes/*.hpp) gnu_include.hpp" << std::endl;
  output << "\t" << "$(CXX) $(CXXFLAGS) $(CXXFLAGS_gnu64) $(OBJ) $(OBJ_gnu64) $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

  output << "install-gnu64: build/prog.elf" << std::endl;
  output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
  output << "uninstall-gnu64:" << std::endl;
  output << "\t" << "rm -vir /usr/local/bin/prog.elf" << std::endl;

  output << "win64: build/prog.exe" << std::endl;
  output << "CXXFLAGS_win64= -I includes -I include.hpp -Wall -g -std=c++17" << std::endl;
  output << "LDFLAGS_win64= $(shell pkg-config --libs --cflags sdl2) -lsfml-graphics" << std::endl;
  output << "SRC_win64= $(wildcard sources/*.cpp) source.cpp" << std::endl;
  output << "OBJDIR_win64= build/obj" << std::endl;
  output << "OBJ_win64= $(foreach src, $(SRC_win64), $(OBJDIR_win64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "run-win64: build/prog.exe" << std::endl;
  output << "\t" << "./$^" << std::endl;
  output << "build/prog.exe:$(OBJ) $(OBJ_win64) $(wildcard includes/*.hpp) include.hpp" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) $(CXXFLAGS) $(CXXFLAGS_win64) $(OBJ) $(OBJ_win64) $(LDFLAGS) $(LDFLAGS_win64) -o $@"
	 << std::endl;

  output << "install-win64: build/prog.exe" << std::endl;
  output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
  output << "uninstall-win64:" << std::endl;
  output << "\t" << "rm -vir /usr/local/bin/prog.exe" << std::endl;

  output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

  output << "$(OBJDIR_gnu64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

  output << "$(OBJDIR_gnu64)/%.o:gnu_sources/%.cpp $(wildcard gnu_sources/*.hpp)" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

  output << "$(OBJDIR_win64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;

  output << "$(OBJDIR_win64)/%.o:sources/%.cpp $(wildcard sources/*.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;

  output << "check:" << std::endl;
  output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_gnu64) $(SRC_win64)" << std::endl;

  output << "clean:" << std::endl;
  output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o $(OBJDIR_win64)/*.o" << std::endl;
  output << "mrproper: clean" << std::endl;
  output << "\t" << "rm -f build/prog.elf build/prog.exe" << std::endl;

  output << ".PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 win64 run-win64 install-win64 uninstall-win64 check clean mrproper" << std::endl;
  MakefileBuilder makefile;
  
  FileCheckerMock checker;
  
  makefile
    .targetObjDir("gnu64", "build/obj")
    .targetToolchainPrefix("gnu64", "")
    .targetExecPath("gnu64", "build/prog.elf")
    .targetSources("gnu64", {"gnu_source.cpp", "gnu_sources"})
    .targetIncludes("gnu64", {"gnu_include.hpp", "gnu_includes"})
    .targetLibs("gnu64", {"-lsfml-graphics", "sdl2"}, checker)
    .targetFlags("gnu64", {"-Wall", "-g", "-std=c++17"})

    .targetObjDir("win64", "build/obj")
    .targetToolchainPrefix("win64", "x86_64-w64-mingw32")
    .targetExecPath("win64", "build/prog.exe")
    .targetSources("win64", {"source.cpp", "sources"})
    .targetIncludes("win64", {"include.hpp", "includes"})
    .targetLibs("win64", {"-lsfml-graphics", "sdl2"}, checker)
    .targetFlags("win64", {"-Wall", "-g", "-std=c++17"});
  
  REQUIRE(output.str() == makefile.build());
}


TEST_CASE("one-target-with-slashes", "[MakefileBuilderTargets]")
{
  std::stringstream output;
  
  output << "CXXFLAGS= -I ./" << std::endl;
  output << "LDFLAGS=" << std::endl;
  output << "SRC= $(wildcard *.cpp)" << std::endl;
  output << "OBJDIR= ." << std::endl;
  output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "win64: build/prog.exe" << std::endl;
  output << "CXXFLAGS_win64= -I includes -I include.hpp -Wall -g -std=c++17" << std::endl;
  output << "LDFLAGS_win64= $(shell pkg-config --libs --cflags sdl2) -lsfml-graphics" << std::endl;
  output << "SRC_win64= $(wildcard sources/*.cpp) source.cpp" << std::endl;
  output << "OBJDIR_win64= build/obj" << std::endl;
  output << "OBJ_win64= $(foreach src, $(SRC_win64), $(OBJDIR_win64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "run-win64: build/prog.exe" << std::endl;
  output << "\t" << "./$^" << std::endl;

  output << "build/prog.exe:$(OBJ) $(OBJ_win64) $(wildcard includes/*.hpp) include.hpp" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) $(CXXFLAGS) $(CXXFLAGS_win64) $(OBJ) $(OBJ_win64) $(LDFLAGS) $(LDFLAGS_win64) -o $@"
	 << std::endl;
  
  output << "install-win64: build/prog.exe" << std::endl;
  output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
  output << "uninstall-win64:" << std::endl;
  output << "\t" << "rm -vir /usr/local/bin/prog.exe" << std::endl;

  output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;
 
  output << "$(OBJDIR_win64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;

  output << "$(OBJDIR_win64)/%.o:sources/%.cpp $(wildcard sources/*.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;

  output << "check:" << std::endl;
  output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_win64)" << std::endl;

  output << "clean:" << std::endl;
  output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_win64)/*.o" << std::endl;
  output << "mrproper: clean" << std::endl;
  output << "\t" << "rm -f build/prog.exe" << std::endl;

  output << ".PHONY: win64 run-win64 install-win64 uninstall-win64 check clean mrproper" << std::endl;
  
  MakefileBuilder makefile;
  FileCheckerMock checker;
  
  makefile
    .targetObjDir("win64", "build/obj//")
    .targetToolchainPrefix("win64", "x86_64-w64-mingw32")
    .targetExecPath("win64", "build/prog.exe")
    .targetSources("win64", {"source.cpp", "sources////"})
    .targetIncludes("win64", {"include.hpp", "includes///"})
    .targetLibs("win64", {"-lsfml-graphics", "sdl2"}, checker)
    .targetFlags("win64", {"-Wall", "-g", "-std=c++17"});
  
  REQUIRE(output.str() == makefile.build());
}

TEST_CASE("bug-one-target", "[MakefileBuilderTargets]")
{
  std::stringstream output;
  
  output << "CXXFLAGS= -I ./" << std::endl;
  output << "LDFLAGS=" << std::endl;
  output << "SRC= $(wildcard *.cpp)" << std::endl;
  output << "OBJDIR= ." << std::endl;
  output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "win64: build/prog.exe" << std::endl;
  output << "CXXFLAGS_win64= -I includes -I include.hpp -Wall -g -std=c++17" << std::endl;
  output << "LDFLAGS_win64= $(shell pkg-config --libs --cflags sdl2) -lsfml-graphics" << std::endl;
  output << "SRC_win64= $(wildcard sources/*.cpp) source.cpp source2.cpp" << std::endl;
  output << "OBJDIR_win64= build/obj" << std::endl;
  output << "OBJ_win64= $(foreach src, $(SRC_win64), $(OBJDIR_win64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "run-win64: build/prog.exe" << std::endl;
  output << "\t" << "./$^" << std::endl;

  output << "build/prog.exe:$(OBJ) $(OBJ_win64) $(wildcard includes/*.hpp) include.hpp" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) $(CXXFLAGS) $(CXXFLAGS_win64) $(OBJ) $(OBJ_win64) $(LDFLAGS) $(LDFLAGS_win64) -o $@"
	 << std::endl;

  output << "install-win64: build/prog.exe" << std::endl;
  output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
  output << "uninstall-win64:" << std::endl;
  output << "\t" << "rm -vir /usr/local/bin/prog.exe" << std::endl;

  output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

  output << "$(OBJDIR_win64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;

  output << "$(OBJDIR_win64)/%.o:sources/%.cpp $(wildcard sources/*.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;

  output << "check:" << std::endl;
  output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_win64)" << std::endl;

  output << "clean:" << std::endl;
  output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_win64)/*.o" << std::endl;
  output << "mrproper: clean" << std::endl;
  output << "\t" << "rm -f build/prog.exe" << std::endl;

  output << ".PHONY: win64 run-win64 install-win64 uninstall-win64 check clean mrproper" << std::endl;
  
  MakefileBuilder makefile;

  FileCheckerMock checker;
  
  makefile
    .targetObjDir("win64", "build/obj")
    .targetToolchainPrefix("win64", "x86_64-w64-mingw32")
    .targetExecPath("win64", "build/prog.exe")
    .targetSources("win64", {"source.cpp", "source2.cpp", "sources"})
    .targetIncludes("win64", {"include.hpp", "includes"})
    .targetLibs("win64", {"-lsfml-graphics", "sdl2"}, checker)
    .targetFlags("win64", {"-Wall", "-g", "-std=c++17"});
  
  REQUIRE(output.str() == makefile.build());
}

TEST_CASE("target-static-lib", "[MakefileBuilderTargets]")
{
  MakefileBuilder makefile;
  makefile.targetExecPath("gnu64", "lib.a");
  
  std::stringstream output;
  output << "CXXFLAGS= -I ./" << std::endl;
  output << "LDFLAGS=" << std::endl;
  output << "SRC= $(wildcard *.cpp)" << std::endl;
  output << "OBJDIR= ." << std::endl;
  output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;
  output << "gnu64: lib.a" << std::endl;
  output << "CXXFLAGS_gnu64=" << std::endl;
  output << "LDFLAGS_gnu64=" << std::endl;
  output << "SRC_gnu64=" << std::endl;
  output << "OBJDIR_gnu64= ." << std::endl;
  output << "OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "run-gnu64: lib.a" << std::endl;
  output << "\t" << "./$^" << std::endl;

  output << "lib.a:$(OBJ) $(OBJ_gnu64)" << std::endl;
  output << "\t" << "ar rvs $@ $^" << std::endl;
  
  output << "install-gnu64: lib.a" << std::endl;
  output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
  output << "uninstall-gnu64:" << std::endl;
  output << "\t" << "rm -vir /usr/local/bin/lib.a" << std::endl;

  output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;
  
  output << "$(OBJDIR_gnu64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $< $(LDFLAGS) $(LDFLAGS_gnu64) -o $@" << std::endl;

  output << "check:" << std::endl;
  output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_gnu64)" << std::endl;

  output << "clean:" << std::endl;
  output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o" << std::endl;
  output << "mrproper: clean" << std::endl;
  output << "\t" << "rm -f lib.a" << std::endl;

  output << ".PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 check clean mrproper" << std::endl;
  
  REQUIRE( output.str() == makefile.build() );
}

TEST_CASE("one-target-with-four-libs", "[MakefileBuilderTargets]")
{
  std::stringstream output;
  
  output << "CXXFLAGS= -I ./" << std::endl;
  output << "LDFLAGS=" << std::endl;
  output << "SRC= $(wildcard *.cpp)" << std::endl;
  output << "OBJDIR= ." << std::endl;
  output << "OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))" << std::endl;
  output << "win64: build/prog.exe" << std::endl;
  output << "CXXFLAGS_win64= -I includes -I include.hpp -Wall -g -std=c++17" << std::endl;
  output << "LDFLAGS_win64= $(shell pkg-config --libs --cflags libpkg) -L libdir -l libfile -libtag" << std::endl;
  output << "SRC_win64= $(wildcard sources/*.cpp) source.cpp" << std::endl;
  output << "OBJDIR_win64= build/obj" << std::endl;
  output << "OBJ_win64= $(foreach src, $(SRC_win64), $(OBJDIR_win64)/$(notdir $(src:%.cpp=%.o)))" << std::endl;

  output << "run-win64: build/prog.exe" << std::endl;
  output << "\t" << "./$^" << std::endl;
  output << "build/prog.exe:$(OBJ) $(OBJ_win64) $(wildcard includes/*.hpp) include.hpp" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) $(CXXFLAGS) $(CXXFLAGS_win64) $(OBJ) $(OBJ_win64) $(LDFLAGS) $(LDFLAGS_win64) -o $@"
	 << std::endl;

  output << "install-win64: build/prog.exe" << std::endl;
  output << "\t" << "cp -vr $^ /usr/local/bin" << std::endl;
  
  output << "uninstall-win64:" << std::endl;
  output << "\t" << "rm -vir /usr/local/bin/prog.exe" << std::endl;

  output << "$(OBJDIR)/%.o:%.cpp" << std::endl;
  output << "\t" << "$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@" << std::endl;

  output << "$(OBJDIR_win64)/%.o:%.cpp $(wildcard *.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;

  output << "$(OBJDIR_win64)/%.o:sources/%.cpp $(wildcard sources/*.hpp)" << std::endl;
  output << "\t" << "x86_64-w64-mingw32-$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_win64) $< $(LDFLAGS) $(LDFLAGS_win64) -o $@" << std::endl;
  
  output << "check:" << std::endl;
  output << "\t" << "cppcheck --enable=all $(SRC) $(SRC_win64)" << std::endl;

  output << "clean:" << std::endl;
  output << "\t" << "rm -f $(OBJ)/*.o $(OBJDIR_win64)/*.o" << std::endl;
  output << "mrproper: clean" << std::endl;
  output << "\t" << "rm -f build/prog.exe" << std::endl;

  output << ".PHONY: win64 run-win64 install-win64 uninstall-win64 check clean mrproper" << std::endl;
  
  MakefileBuilder makefile;
  FileCheckerMock checker;
  checker.dirs.push_back("libdir");
  checker.files.push_back("libfile");

  makefile
    .targetObjDir("win64", "build/obj")
    .targetToolchainPrefix("win64", "x86_64-w64-mingw32")
    .targetExecPath("win64", "build/prog.exe")
    .targetSources("win64", {"source.cpp", "sources"})
    .targetIncludes("win64", {"include.hpp", "includes"})
    .targetLibs("win64", {"libpkg", "libfile", "-libtag",  "libdir"}, checker)
    .targetFlags("win64", {"-Wall", "-g", "-std=c++17"});
  
  REQUIRE(output.str() == makefile.build());
}
