#ifndef FILE_READER_MOCK_HPP
#define FILE_READER_MOCK_HPP
#include <vector>
#include "../src/FileReader.hpp"

struct FileReaderMock : public FileReader
{
  virtual std::string from(std::string const& path) const override
  {
    return content;
  }

  std::string content;
};

#endif
