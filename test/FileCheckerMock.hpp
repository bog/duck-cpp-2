#ifndef FILE_CHECKER_MOCK
#define FILE_CHECKER_MOCK
#include "../src/FileChecker.hpp"

struct FileCheckerMock : public FileChecker
{
  virtual bool exists(std::string const& dir) const override
  {
    auto itr = std::find(ok.begin(), ok.end(), dir);
    return itr != ok.end();
  }

  virtual bool isFile(std::string const& dir) const override
  {
    auto itr = std::find(files.begin(), files.end(), dir);
    return itr != files.end();
  }
  
  virtual bool isDir(std::string const& dir) const override
  {
    auto itr = std::find(dirs.begin(), dirs.end(), dir);
    return itr != dirs.end();

  }

  std::vector<std::string> ok;
  std::vector<std::string> dirs;
  std::vector<std::string> files;
};

#endif
