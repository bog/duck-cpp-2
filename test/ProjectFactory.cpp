#include <catch.hpp>
#include "../src/ProjectFactory.hpp"
#include "../src/YamlFactory.hpp"
#include "FileCreatorMock.hpp"
#include "FileCheckerMock.hpp"

TEST_CASE("project-simple", "[ProjectFactory]")
{
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;

  YamlFactory yaml_factory;
  ProjectFactory project(file_creator, file_checker, yaml_factory);

  project.initSimple("name");

  std::vector<std::string> v0 = file_creator.dirsCreated();
  std::vector<std::pair<std::string, std::string>> v1 = file_creator.filesCreated();

  REQUIRE( 4 == v0.size() );
  REQUIRE( std::any_of(v0.begin(), v0.end(), [](std::string const& str){ return str == "build"; }) );
  REQUIRE( std::any_of(v0.begin(), v0.end(), [](std::string const& str){ return str == "build/bin"; }) );
  REQUIRE( std::any_of(v0.begin(), v0.end(), [](std::string const& str){ return str == "build/obj"; }) );
  REQUIRE( std::any_of(v0.begin(), v0.end(), [](std::string const& str){ return str == "src"; }) );
  
  REQUIRE( 2 == v1.size() );
  REQUIRE(std::any_of(v1.begin(), v1.end(),
		      [&yaml_factory](std::pair<std::string, std::string> p)
		      {
			return p.first == "duck.yml" && p.second == yaml_factory.simple("name");
		      }));

  std::stringstream source;
  source << "#include <iostream>" << std::endl;
  source << "" << std::endl;
  source << "" << std::endl;
  source << "int main(int argc, char** argv)" << std::endl;
  source << "{" << std::endl;
  source << "\t" << "std::cout << \"name\" << \" !\" << std::endl;" << std::endl;
  source << "\t" << "return 0;" << std::endl;
  source << "}" << std::endl;
  
  REQUIRE(std::any_of(v1.begin(), v1.end(),
		      [&source](std::pair<std::string, std::string> p)
		      {
			return p.first == "src/name.cpp" && p.second == source.str();
		      }));
}


TEST_CASE("project-simple-build-already-exists", "[ProjectFactory]")
{
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  file_checker.ok.push_back("build");


  YamlFactory yaml_factory;
  ProjectFactory project(file_creator, file_checker, yaml_factory);

  REQUIRE_THROWS( project.initSimple("name") );
}

TEST_CASE("project-simple-build-bin-already-exists", "[ProjectFactory]")
{
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  file_checker.ok.push_back("build/bin");

  YamlFactory yaml_factory;
  ProjectFactory project(file_creator, file_checker, yaml_factory);

  REQUIRE_THROWS( project.initSimple("name") );
}

TEST_CASE("project-simple-build-obj-already-exists", "[ProjectFactory]")
{
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  file_checker.ok.push_back("build/obj");

  YamlFactory yaml_factory;
  ProjectFactory project(file_creator, file_checker, yaml_factory);

  REQUIRE_THROWS( project.initSimple("name") );
}

TEST_CASE("project-simple-src-already-exists", "[ProjectFactory]")
{
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  file_checker.ok.push_back("src");

  YamlFactory yaml_factory;
  ProjectFactory project(file_creator, file_checker, yaml_factory);

  REQUIRE_THROWS( project.initSimple("name") );
}
