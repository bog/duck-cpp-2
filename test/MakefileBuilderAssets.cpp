#include <sstream>
#include <catch.hpp>
#include "../src/MakefileBuilder.hpp"

TEST_CASE("fully-specified", "[MakefileBuilderAssets]")
{
  MakefileBuilder makefile;
  makefile
    .assetDirs({"home", "usr", "share"})
    .assetDirs({"etc", "sys", "run", "opt"});

  Asset a0 = makefile.getAssets()[0];
  Asset a1 = makefile.getAssets()[1];
  
  REQUIRE( 3 == a0.dirs.size() );
  REQUIRE( 4 == a1.dirs.size() );

  REQUIRE("home" == a0.dirs[0]);
  REQUIRE("usr" == a0.dirs[1]);
  REQUIRE("share" == a0.dirs[2]);

  REQUIRE("etc" == a1.dirs[0]);
  REQUIRE("sys" == a1.dirs[1]);
  REQUIRE("run" == a1.dirs[2]);
  REQUIRE("opt" == a1.dirs[3]);
}

TEST_CASE("fully-specified-with-slashes", "[MakefileBuilderAssets]")
{
  MakefileBuilder makefile;
  makefile
    .assetDirs({"home///", "usr//", "share/"})
    .assetDirs({"etc//", "sys/", "run///", "opt//"});

  Asset a0 = makefile.getAssets()[0];
  Asset a1 = makefile.getAssets()[1];
  
  REQUIRE( 3 == a0.dirs.size() );
  REQUIRE( 4 == a1.dirs.size() );

  REQUIRE("home" == a0.dirs[0]);
  REQUIRE("usr" == a0.dirs[1]);
  REQUIRE("share" == a0.dirs[2]);

  REQUIRE("etc" == a1.dirs[0]);
  REQUIRE("sys" == a1.dirs[1]);
  REQUIRE("run" == a1.dirs[2]);
  REQUIRE("opt" == a1.dirs[3]);
}
