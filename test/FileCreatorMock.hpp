#ifndef FILE_CREATOR_MOCK_HPP
#define FILE_CREATOR_MOCK_HPP
#include <vector>
#include "../src/FileCreator.hpp"

struct FileCreatorMock : public FileCreator
{
  FileCreatorMock()
  {
  }
  
  virtual void createDir(std::string const& name) override
  {
    m_dirs.push_back(name);
  }
  
  virtual void createFile(std::string const& name, std::string const& content) override
  {
    m_files.push_back({name, content});
  }

  virtual void remove(std::string const& name) override
  {
    m_removed.push_back(name);
  }

  std::vector<std::string> dirsCreated() const
  {
    return m_dirs;
  }

  std::vector<std::pair<std::string, std::string>> filesCreated() const
  {
    return m_files;
  }

  std::vector<std::string> removed() const
  {
    return m_removed;
  }
  
  virtual ~FileCreatorMock()
  {
  }

  std::vector<std::string> m_dirs;
  std::vector<std::pair<std::string, std::string>> m_files;
  std::vector<std::string> m_removed;
};

#endif
