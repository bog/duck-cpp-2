#include <map>
#include <sstream>
#include <catch.hpp>
#include "../src/MakefileBuilder.hpp"
#include "../src/MakefileChecker.hpp"
#include "../src/YamlReader.hpp"
#include "../src/Sections.hpp"
#include "FileCheckerMock.hpp"

TEST_CASE("make-yaml-checker-empty", "[IMakefileBuilderYamlReader]")
{
  YamlReader yaml { "" };
  MakefileBuilder builder;
  FileCheckerMock fchecker;
  fchecker.ok.push_back("/usr/local/bin");
  fchecker.ok.push_back("/usr/local/share");
  fchecker.ok.push_back(".");
  fchecker.ok.push_back("");
  
  MakefileChecker checker {fchecker, builder};
  
  REQUIRE_NOTHROW( builder.yamlReader(yaml, fchecker) );

  std::cout<< "--------------------------------" <<std::endl;

  REQUIRE(checker.validate());
}


TEST_CASE("make-yaml-bug-toolchain", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "targets:" << std::endl;
  input << "  - target:" << std::endl;
  input << "      name: hello" << std::endl;
  input << "      toolchain-prefix: aze" << std::endl;
  
  YamlReader yaml { input.str() };
  MakefileBuilder builder;
  FileCheckerMock fchecker;
  fchecker.ok.push_back("/usr/local/bin");
  fchecker.ok.push_back("/usr/local/share");
  fchecker.ok.push_back(".");
  fchecker.ok.push_back("");
  
  MakefileChecker checker {fchecker, builder};  
  
  REQUIRE_NOTHROW( builder.yamlReader(yaml, fchecker) );

  REQUIRE(checker.validate());
}

TEST_CASE("make-yaml-bug-targets", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "targets:" << std::endl;
  input << "  - target:" << std::endl;
  input << "      name: hello" << std::endl; 
 
  YamlReader yaml { input.str() };
  MakefileBuilder builder;
  FileCheckerMock fchecker;
  fchecker.ok.push_back("/usr/local/bin");
  fchecker.ok.push_back("/usr/local/share");
  fchecker.ok.push_back(".");
  fchecker.ok.push_back("");
  
  MakefileChecker checker {fchecker, builder}; 
  
  REQUIRE_NOTHROW( builder.yamlReader(yaml, fchecker) );

  REQUIRE(checker.validate());
}

TEST_CASE("make-yaml-exec-path-not-unique-error", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "targets:" << std::endl;
  input << "  - target:" << std::endl;
  input << "      name: hello" << std::endl;
  input << "      obj-dir: world" << std::endl;
  input << "      exec-path: aze" << std::endl; 
  input << "  - target:" << std::endl;
  input << "      name: hello2" << std::endl;
  input << "      obj-dir: world2" << std::endl;
  input << "      exec-path: aze" << std::endl; 
 
  YamlReader yaml { input.str() };
  MakefileBuilder builder;
  FileCheckerMock fchecker;
  fchecker.ok.push_back("/usr/local/bin");
  fchecker.ok.push_back("/usr/local/share");
  fchecker.ok.push_back("world");
  fchecker.ok.push_back("world2");
  fchecker.ok.push_back(".");
  fchecker.ok.push_back("");
  
  MakefileChecker checker {fchecker, builder};
  

  REQUIRE_NOTHROW( builder.yamlReader(yaml, fchecker) );

  REQUIRE(checker.validate());
  REQUIRE(1 == checker.errors().size());
  REQUIRE("W: exec-path should be unique." == checker.errors().back());
}

TEST_CASE("make-yaml-target-full-empty", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "targets:" << std::endl;
  input << "  - target:" << std::endl;
  input << "      name:" << std::endl;
  input << "      toolchain-prefix:" << std::endl;
  input << "      exec-path:" << std::endl;
  input << "      sources:" << std::endl;
  input << "      includes:" << std::endl;
  input << "      libs:" << std::endl;
  input << "      flags:" << std::endl;
  input << "      obj-dir:" << std::endl;
  
  YamlReader yaml { input.str() };
  MakefileBuilder builder;
  FileCheckerMock fchecker;
  fchecker.ok.push_back("/usr/local/bin");
  fchecker.ok.push_back("/usr/local/share");
  fchecker.ok.push_back(".");
  fchecker.ok.push_back("");
  
  MakefileChecker checker {fchecker, builder};
  
  REQUIRE_NOTHROW( builder.yamlReader(yaml, fchecker) );
  
  REQUIRE(checker.validate());
}

TEST_CASE("make-yaml-project-full-empty", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "project:" << std::endl;
  input << "  name:" << std::endl;
  input << "  authors:" << std::endl;
  input << "  license:" << std::endl;
  input << "  version:" << std::endl;
  
  YamlReader yaml { input.str() };
  MakefileBuilder builder;
  FileCheckerMock fchecker;
  fchecker.ok.push_back("/usr/local/bin");
  fchecker.ok.push_back("/usr/local/share");
  fchecker.ok.push_back(".");
  fchecker.ok.push_back("");

  MakefileChecker checker {fchecker, builder};
  
  REQUIRE_NOTHROW( builder.yamlReader(yaml, fchecker) );

  REQUIRE(checker.validate());
}

TEST_CASE("make-yaml-install-full-empty", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "install:" << std::endl;
  input << "  bin-dir:" << std::endl;
  input << "  asset-dir:" << std::endl;
  
  YamlReader yaml { input.str() };
  MakefileBuilder builder;
  FileCheckerMock fchecker;
  fchecker.ok.push_back("/usr/local/bin");
  fchecker.ok.push_back("/usr/local/share");
  fchecker.ok.push_back(".");
  fchecker.ok.push_back("");
  
  MakefileChecker checker {fchecker, builder};
 
  
  REQUIRE_NOTHROW( builder.yamlReader(yaml, fchecker) );

  REQUIRE(checker.validate());
}


TEST_CASE("make-yaml-common-full-empty", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "common:" << std::endl;
  input << "  sources:" << std::endl;
  input << "  includes:" << std::endl;
  input << "  libs:" << std::endl;
  input << "  flags:" << std::endl;
  input << "  obj-dir:" << std::endl;
  
  YamlReader yaml { input.str() };
  MakefileBuilder builder;
  FileCheckerMock fchecker;
  fchecker.ok.push_back("/usr/local/bin");
  fchecker.ok.push_back("/usr/local/share");
  fchecker.ok.push_back(".");
  fchecker.ok.push_back("");
  
  MakefileChecker checker {fchecker, builder};
    
  REQUIRE_NOTHROW( builder.yamlReader(yaml, fchecker) );

  REQUIRE(checker.validate());
}

TEST_CASE("make-yaml-assets-full-empty", "[IMakefileBuilderYamlReader]")
{
  std::stringstream input;
  input << "assets:" << std::endl;
  input << "  - asset:" << std::endl;
  input << "      dirs:" << std::endl;
  
  YamlReader yaml { input.str() };
  MakefileBuilder builder;
  FileCheckerMock fchecker;
  fchecker.ok.push_back("/usr/local/bin");
  fchecker.ok.push_back("/usr/local/share");
  fchecker.ok.push_back(".");
  fchecker.ok.push_back("");

  
  MakefileChecker checker {fchecker, builder};
  
  REQUIRE_NOTHROW( builder.yamlReader(yaml, fchecker) );

  REQUIRE(checker.validate());
}

