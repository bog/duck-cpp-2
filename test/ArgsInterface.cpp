#include <catch.hpp>
#include "FileCreatorMock.hpp"
#include "FileCheckerMock.hpp"
#include "FileReaderMock.hpp"
#include "../src/YamlFactory.hpp"
#include "../src/ArgsParser.hpp"
#include "../src/ProjectFactory.hpp"
#include "../src/ArgsInterface.hpp"

TEST_CASE("ai-bad-arg", "[ArgsInterface]")
{
  ArgsParser parser({"hello"});

  YamlFactory yaml;
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  FileReaderMock file_reader;
  ProjectFactory project(file_creator, file_checker, yaml);
    
  ArgsInterface interface(parser, project, file_creator, file_checker, file_reader);
  REQUIRE_THROWS( interface.execute() );
}

TEST_CASE("ai-init", "[ArgsInterface]")
{
  ArgsParser parser({"init", "name"});

  YamlFactory yaml;
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  FileReaderMock file_reader;
  file_checker.ok.push_back("/usr/local/share");
  file_checker.ok.push_back("/usr/local/bin");
  file_checker.ok.push_back(".");
  file_checker.ok.push_back("");
  
  ProjectFactory project(file_creator, file_checker, yaml);

  ArgsInterface interface(parser, project, file_creator, file_checker, file_reader);
  REQUIRE_NOTHROW( interface.execute() );

  REQUIRE(3 == file_creator.filesCreated().size());
  REQUIRE("src/name.cpp" == file_creator.filesCreated()[0].first);
  REQUIRE("duck.yml" == file_creator.filesCreated()[1].first);
  REQUIRE("Makefile" == file_creator.filesCreated()[2].first);
  
  REQUIRE(4 == file_creator.dirsCreated().size());
  REQUIRE("build" == file_creator.dirsCreated()[0]);
  REQUIRE("build/bin" == file_creator.dirsCreated()[1]);
  REQUIRE("build/obj" == file_creator.dirsCreated()[2]);
  REQUIRE("src" == file_creator.dirsCreated()[3]);  
}

TEST_CASE("ai-init-no-name", "[ArgsInterface]")
{
  ArgsParser parser({"init"});

  YamlFactory yaml;
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  FileReaderMock file_reader;
  ProjectFactory project(file_creator, file_checker, yaml);
    
  ArgsInterface interface(parser, project, file_creator, file_checker, file_reader);
  
  REQUIRE_THROWS(interface.execute());
  REQUIRE( 0 == file_creator.dirsCreated().size() );
  REQUIRE( 0 == file_creator.filesCreated().size() );
  REQUIRE( 0 == file_creator.removed().size() );
}

TEST_CASE("ai-update-error", "[ArgsInterface]")
{
  ArgsParser parser({"update"});

  YamlFactory yaml;
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  FileReaderMock file_reader;  
  
  ProjectFactory project(file_creator, file_checker, yaml);
  
  ArgsInterface interface(parser, project, file_creator, file_checker, file_reader);
  REQUIRE_THROWS( interface.execute() );

  REQUIRE(0 == file_creator.filesCreated().size());
  REQUIRE(0 == file_creator.removed().size());
}

TEST_CASE("ai-update", "[ArgsInterface]")
{
  ArgsParser parser({"update"});

  YamlFactory yaml;
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  FileReaderMock file_reader;
  file_checker.ok.push_back("/usr/local/bin");
  file_checker.ok.push_back("/usr/local/share");
  file_checker.ok.push_back("duck.yml");
  file_checker.ok.push_back("Makefile");
  file_checker.ok.push_back(".");
  file_checker.ok.push_back("");
  
  ProjectFactory project(file_creator, file_checker, yaml);
  
  ArgsInterface interface(parser, project, file_creator, file_checker, file_reader);
  REQUIRE_NOTHROW( interface.execute() );

  REQUIRE(1 == file_creator.removed().size());
  REQUIRE("Makefile" == file_creator.removed()[0]);
  REQUIRE(1 == file_creator.filesCreated().size());  
  REQUIRE("Makefile" == file_creator.filesCreated()[0].first);
}

TEST_CASE("ai-empty", "[ArgsInterface]")
{
  ArgsParser parser({});

  YamlFactory yaml;
  FileCreatorMock file_creator;
  FileCheckerMock file_checker;
  FileReaderMock file_reader;
  file_checker.ok.push_back("/usr/local/bin");
  file_checker.ok.push_back("/usr/local/share");
  file_checker.ok.push_back("duck.yml");
  file_checker.ok.push_back("Makefile");
  file_checker.ok.push_back(".");
  file_checker.ok.push_back("");
  
  ProjectFactory project(file_creator, file_checker, yaml);
  
  ArgsInterface interface(parser, project, file_creator, file_checker, file_reader);
  REQUIRE_NOTHROW( interface.execute() );
}
