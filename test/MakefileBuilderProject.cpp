#include <sstream>
#include <catch.hpp>
#include "../src/MakefileBuilder.hpp"

TEST_CASE("project-default", "[MakefileBuilderProject]")
{
  MakefileBuilder makefile;
  makefile
    .projectName("")
    .projectAuthors({})
    .projectLicense("")
    .projectVersion("");

  Project project = makefile.getProject();
  
  REQUIRE("new-project" == project.name);
  REQUIRE(0 == project.authors.size());
  REQUIRE("MIT" == project.license);
  REQUIRE("0" == project.version);
}

TEST_CASE("project-fully-specified", "[MakefileBuilderProject]")
{
  MakefileBuilder makefile;
  makefile
    .projectName("My Project")
    .projectAuthors({"paul", "maxime"})
    .projectLicense("GNU GPLv3")
    .projectVersion("3.14");

  Project project = makefile.getProject();
  REQUIRE("My Project" == project.name);
  REQUIRE(2 == project.authors.size());
  REQUIRE("paul" == project.authors[0]);
  REQUIRE("maxime" == project.authors[1]);
  REQUIRE("GNU GPLv3" == project.license);
  REQUIRE("3.14" == project.version);
}
